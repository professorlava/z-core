==================
ZCORE coding Style
==================

The coding style for the zcore library mostly follows those rules outlined
by the linux kernel development community. Please read the kernel
CodingStyle documentation here:

	https://www.kernel.org/doc/Documentation/CodingStyle

There are several addendums and exceptions to the kernel style which should 
be observed while writing code for zcore. They are outlined as follows:

Addendums
---------

I. Const Correctness & Const Placement
	Simply put, const correctness is good for the environment. It helps
	the compiler more easily determine how to optimize function calls
	and unroll loops and such, therefor saving electrons.

	Const correctness is another layer over top of type that ensures
	the mutability of a symbol matches with the expected mutability of
	a parameter into a function. This is extremely valuable for pointer
	operations. It helps create self documenting code and encourages
	proper encapsulation.

	Read a variable declaration from left to right.

	.. sourcecode :: c

		int const         a; // A is a constant int
		int *             b; // B is a ptr to int
		int const *       c; // C is a ptr to constant int
		int const * const d; // D is a const ptr to a const int
		int ** const      e; // E is a const ptr to a ptr to an int
		int * const *     f; // F is a ptr to a const ptr to an int

	Note that a containing structure that is const can be considered
	to have all const members, except that the value pointed to by
	pointers remains mutable unless the struct definition defined it as
	const.

II. Inlining
	The kernel style guide covers inlining, but I will reiterate;

		Abundant use of the inline keyword leads to a much bigger
		kernel, which in turn slows the system as a whole down, due
		to a bigger icache footprint for the CPU and simply because
		there is less memory available for the pagecache. Just think
		about it; a pagecache miss causes a disk seek, which easily
		takes 5 milliseconds. There are a LOT of cpu cycles that can
		go into these 5 milliseconds.

		A reasonable rule of thumb is to not put inline at functions
		that have more than 3 lines of code in them. An exception to
		this rule are the cases where a parameter is known to be a
		compiletime constant, and as a result of this constantness you
		*know* the compiler will be able to optimize most of your
		function away at compile time.

Exceptions
----------

III. Spacing (Chapter 3)
	When declaring pointer data or a function that returns a pointer
	type, the preferred use of '*' is separated but adjacent to the
	type name.  Examples:

	.. sourcecode :: c

		char const * const linux_banner;
		unsigned long long memparse(char * ptr, char ** retptr);
		char *             match_strdup(substring_t * s);

	The rational here is there the '*' is part of the type, even though
	in C a comma separated declaration requires the final indirection
	to be after the comma with the name. This is likely the reason for
	the kernel style. However, the kernel style also discourages comma
	separated declaration of variables, making this moot! More important
	is the consistency of the type name. Since const can be applied in
	between two '*' and const is *definitely* part of the type we place
	'*' with the type name.

IV. Reference Counting (Chapter 11):
	This is a generalized library, not being designed for a specific
	use or application. Stuff like locking or reference counting should
	actually be *avoided*; in favor of allowing the users of the library
	to do locking or counting of objects they may receive from it.

	caveat
	  If a module is ever added to the library that abstracts
	  reference counting or locking, these modules can perform these
	  actions :)

V. Logging and Alloc (Chapters 13 & 14):
	The kernel style document addresses special logging and alloc
	functions which are not relevant to this library.

Documentation
-------------

I have not yet choosen a Auto-gen documentation system yet, so for now,
stick to java-doc style markup, as it is compatible with doxygen and many
other auto-doc generation tools.

