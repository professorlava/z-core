======
Z Core
======

A corpus of common or generalized code for reuse in other projects.

Mostly recipes, macros, containers, and generic helper functions.


Contributors
============

Zachary Schoenstadt
