macro (append VAR ADDEND)
  set (_STRING ${${VAR}})
  set (${VAR} "${_STRING} ${ADDEND}")
endmacro (append)

macro (append_if VAR COND ADDEND)
  if (${COND})
    append (${VAR} ${ADDEND})
  endif (${COND})
endmacro (append_if)
