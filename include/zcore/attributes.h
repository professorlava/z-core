#ifndef ZCORE_ATTRIBUTES_H
#define ZCORE_ATTRIBUTES_H

/**
 * Ensure a struct is treated as packed
 */
#if defined(__GNUC__)
#  define packed(decl) decl __attribute__((packed))
#elif defined(_MSC_VER)
#  define packed(decl) __pragma(pack(push,1)) decl __pragma(pack(pop))
#else
#  define packed(decl) decl
#endif

/** attr_inline
 * Generally, functions are not inlined unless optimization is specified.
 * For functions declared inline, this attribute inlines the function
 * independent of any restrictions that otherwise apply to inlining.
 * Failure to inline such a function is diagnosed as an error. Note that if
 * such a function is called indirectly the compiler may or may not inline
 * it depending on optimization level and a failure to inline an indirect
 * call may or may not be diagnosed.
 */
#if defined(__GNUC__)
#  define attr_inline inline __attribute__((always_inline))
#elif defined(__MSC_VER)
#  define attr_inline inline __forceinline
#else
#  define attr_inline inline
#endif

/** attr_noinline
 * This function attribute prevents a function from being considered for
 * inlining. If the function does not have side-effects, there are
 * optimizations other than inlining that cause function calls to be
 * optimized away, although the function call is live. To keep such calls
 * from being optimized away, put ` asm("");` inside the function to create
 * a special side effect
 */
#if defined(__GNUC__)
#  define attr_noinline __attribute__((noinline))
#elif defined(__MSC_VER)
#  define attr_noinline __declspec(noinline)
#else
#  define attr_noinline
#endif


/** attr_const
 * Many functions do not examine any values except their arguments, and
 * have no effects except the return value. Basically this is just slightly
 * more strict class than the pure attribute below, since function is not
 * allowed to read global memory.
 *
 * Note that a function that has pointer arguments and examines the data
 * pointed to must not be declared const. Likewise, a function that calls a
 * non-const function usually must not be const. It does not make sense for
 * a const function to return void.
 */
#if defined(__GNUC__)
#  define attr_const __attribute__((const))
#elif defined(__MSC_VER)
#  define attr_const __declspec(const)
#else
#  define attr_const
#endif

/** attr_pure
 * Many functions have no effects except the return value and their return
 * value depends only on the parameters and/or global variables. Such a
 * function can be subject to common subexpression elimination and loop
 * optimization just as an arithmetic operator would be. These functions
 * should be declared with the attribute pure.
 */
#if defined(__GNUC__)
#  define attr_pure __attribute__((pure))
#else
#  define attr_pure
#endif

/** attr_section
 * Specifies what section of the binary to place the symbol
 */
#if defined(__GNUC__)
#  define attr_section(a) __attribute__((section(a)))
#elif defined(__MSC_VER)
#  define attr_section(a) __declspec(code_seg(a))
#else
#  define attr_section
#endif

/** attr_unused
 * will not remove from binary if unused
 */
#if defined(__GNUC__)
#  define attr_unused __attribute__((unused))
#elif defined(__MSC_VER)
#  define attr_unused __declspec(unused)
#else
#  define attr_unused
#endif

/** attr_noreturn */
#if __STDC_VERSION__ >= 201112L
#  define attr_noreturn _Noreturn
#else
#  if defined(__GNUC__)
#    define attr_noreturn __attribute__((noreturn))
#  elif defined(__MSC_VER)
#    define attr_noreturn __declspec(noreturn)
#  else
#    define attr_noreturn
#  endif
#endif

#endif/*ZCORE_ATTRIBUTES_H*/
