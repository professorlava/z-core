#ifndef ZCORE_COMPARER_H
#define ZCORE_COMPARER_H

#include "zcore/attributes.h"

#ifdef __cplusplus
extern "C" {
#endif

struct zcore_comparer {
	/**
	 * Compare two things.
	 *
	 * @param the comparer that contains the called function pointer.
	 * @param a first thing to be compared
	 * @param b second thing to be compared
	 * @return < 0 if a < b. \n
	 *         = 0 if a = b. \n
	 *         > 0 if a > b.
	 */
	int (* compare)(
		struct zcore_comparer const * the,
		void const * a,
		void const * b);
};

#define ZCORE_COMPARER_INIT(cmp) \
	(struct zcore_comparer) { \
		.compare=(cmp),\
	}

#define ZCORE_COMPARER_DECLARE(name) struct zcore_comparer name

#define ZCORE_COMPARER_DEFINE(name, cmp) \
	ZCORE_COMPARER_DECLARE(name) = ZCORE_COMPARER_INIT(cmp)

/**
 * Compare two things by applying comparer.
 *
 * @param the comparer to apply
 * @param a first thing to compare
 * @param b second thing to compare
 * @return < 0 if a < b. \n
 *         = 0 if a = b. \n
 *         > 0 if a > b.
 */
static attr_inline int
zcore_compare(struct zcore_comparer const * the,
              void const * a, void const * b)
{
	return the->compare(the, a, b);
}

#ifdef __cplusplus
}
#endif

#endif/*ZCORE_COMPARER_H*/
