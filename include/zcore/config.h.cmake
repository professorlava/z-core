#ifndef ZCORE_CONFIG_H
#define ZCORE_CONFIG_H

#define ZCORE_VERSION_MAJOR @ZCORE_VERSION_MAJOR@
#define ZCORE_VERSION_MINOR @ZCORE_VERSION_MINOR@
#define ZCORE_VERSION_PATCH @ZCORE_VERSION_PATCH@
#define ZCORE_VERSION_STR "@ZCORE_VERSION@"

/* Bits in a long */
#define ZCORE_BITS_PER_LONG @BITS_PER_LONG@

/* Bits in a pointer */
#define ZCORE_BITS_PER_PTR @BITS_PER_PTR@

/* Initial height of avlarr used to implement oset */
#define ZCORE_OSET_INITIAL_HEIGHT @ZCORE_OSET_INITIAL_HEIGHT@

/* Initial height of avlarr used to implement omap */
#define ZCORE_OMAP_INITIAL_HEIGHT @ZCORE_OMAP_INITIAL_HEIGHT@

/* Initial height of avlarr used to implement prioq */
#define ZCORE_PRIOQ_INITIAL_HEIGHT @ZCORE_PRIOQ_INITIAL_HEIGHT@

/* Associative containers ensure that values without keys are always 0 */
#cmakedefine ZCORE_SANITARY_CONTAINERS 1

#endif/*ZCORE_CONFIG_H*/
