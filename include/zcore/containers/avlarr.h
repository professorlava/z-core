/*
  Array base AVL tree
  Copyright (C) 2015, Zachary Schoenstadt

  All rights reserved.
*/

/** \file
 *
 * Array based AVL tree.
 *
 * An AVL tree is a balanced binary search tree. Named after their
 * inventors, Adelson-Velskii and Landis, they were the *first* dynamically
 * balanced trees to be proposed. Like red-black trees, they are not
 * perfectly balanced, but pairs of sub-trees differ in height by at
 * most 1, maintaining an O(log n) search time.
 *
 * Since this implementation utilizes an array in memory to hold all the
 * key and value nodes, simple calculations can be used to obtain the
 * parents and children. Using this, many of the usually recursive algorithms
 * can easily be reduced to simple loops, and certain behaviors such as
 * insertion and removal can leverage the ability to calculate parents for
 * 'semi-threaded' behavior.
 *
 * Faster than red-black tree for lookup-intensive applications
 *
 * Applications include dictionaries, maps, ordered sets, etc.
 */

#ifndef ZCORE_CONTAINERS_AVL_ARRAY_H
#define ZCORE_CONTAINERS_AVL_ARRAY_H

#include "zcore/stdinc.h"
#include "zcore/comparer.h"
#include "zcore/destructor.h"
#include "zcore/memory.h"

#ifdef __cplusplus
extern "C" {
#endif

#define ZCORE_AVLARR_H2C(x) (((x) >= 0) ? ((2ULL << ((size_t)(x))) - 1ULL) : 0)
#define ZCORE_AVLARR_NULL_HEIGHT -1

/**
 * Since the array needs to balance itself, it is neccessary to hold a pointer
 * to the comparison function. But array is not responsible for managing
 * internals of its values or keys, only the values of the pointers, so dtor
 * is needed on explicit destruction to handle the values.
 */
struct zcore_avlarr {
	void ** keys;      /**< capacity * sizeof(void*) */
	void ** values;    /**< capacity * sizeof(void*) (if not null)*/
	int8_t * heights;  /**< capacity * sizeof(int8_t) */
	size_t count;      /**< Number of key-value pairs currently held */
	size_t capacity;   /**< index length of keys & values arrays */
	struct zcore_comparer const * key_cmp;
	struct zcore_memoryif const * mi;
};

struct zcore_avlarr_destructor {
	struct zcore_destructor base;
	struct zcore_destructor const * key_dtor;
	struct zcore_destructor const * val_dtor;
};

/**
 * Construct a Resizing zcore_avlarr.
 *
 * @param  the zcore_avlarr to construct.
 * @param  store_values
 *           Whether or not the avlarr stores values determines if it
 *           behaves like a Set or like an Map.
 * @param  initial_height
 *           Initial max physical height of the tree. Must be at least 0.
 *         Note: int type is more than sufficient to hold the height
 *           on any platform, since the relationship between height
 *           and mem-use is
 *           mem=(sizeof(char)+2*sizeof(void*))*(pow(2,(h+1))-1),
 *           ie: max addressable height on a 32bit machine is only 27,
 *           which would comsume just over 3.2 Gb of memory. A char will carry
 *           this implementation FAR into the future.
 * @param  key_cmp Comparator for keys type.
 * @param  mi
 *           Memory Interface used to dynamically allocate and resize
 *           key, value, and height arrays. If NULL is given, stdmi is used.
 * @return 0 on success\n
 *         -EINVAL if any input is invalid or ill formed\n
 *         -ENOMEM if keys, values, or heights arrays failed to allocate.
 */
int
zcore_avlarr_construct(struct zcore_avlarr * the,
                       bool store_values,
                       int8_t initial_height,
                       struct zcore_comparer const * key_cmp,
                       struct zcore_memoryif const * mi);

/**
 * Destruct an zcore_avlarr.
 * First applying passed dtor to every remaining element in the tree.
 *
 * @param  the zcore_avlarr to destruct
 * @param  key_dtor
 *           Fn to handle the transfer of ownership of the value
 *           nodes. This can mean either freeing allocated memory that is no
 *           longer needed, or nominally ensuring the memory is still
 *           reachable elsewhere.
 * @param  val_dtor
 *           Fn to handle the transfer of ownership of the value
 *           nodes. This can mean either freeing allocated memory that is no
 *           longer needed, or nominally ensuring the memory is still
 *           reachable elsewhere.\
 * @param  data Extra data passed into node_dtor.
 */
void
zcore_avlarr_destruct(struct zcore_avlarr * the,
                      struct zcore_destructor const * key_dtor,
                      struct zcore_destructor const * val_dtor);

/**
 * Implements the zcore_destructor::destruct function for the avlarr struct.
 *
 * The following are all equivalent:
 *
 *     ZCORE_AVLARR_DESTRUCTOR_DEFINE(thedtor, key_dtor, val_dtor);
 *     zcore_destruct(&thedtor, the);
 *
 *     ZCORE_AVLARR_DESTRUCTOR_DEFINE(thedtor, key_dtor, val_dtor);
 *     zcore_avlarr_destructor(&thedtor, the);
 *
 *     zcore_avlarr_destruct(the, key_dtor, val_dtor);
 */
void
zcore_avlarr_destructor(struct zcore_destructor const * the, void * ptr);

static attr_inline void
zcore_avlarr_destructor_initialize(struct zcore_avlarr_destructor * the,
                                   struct zcore_destructor const * key_dtor,
                                   struct zcore_destructor const * val_dtor)
{
	the->base = ZCORE_DESTRUCTOR_INIT(zcore_avlarr_destructor);
	the->key_dtor = key_dtor;
	the->val_dtor = val_dtor;
}

/**
 * Nuke the contents of the internal storage without freeing them.
 *
 * @param the avlarr to flush
 */
void
zcore_avlarr_flush(struct zcore_avlarr * the);

/**
 * Insert a node into the tree.
 * If the node is stack allocated make sure it is removed from the tree
 * before it loses context.
 *
 * @param  the zcore_avlarr to insert into
 * @param  key
 *           Key used to compare the value-nodes. If no cmp function
 *           was provided the key is interpreted as an unsigned word rather
 *           than as a pointer to memory. A value of 0/NULL is invalid.
 * @param  node Data to store
 * @return 0 on success\n
 *         -EINVAL if key is NULL.\n
 *         -EAGAIN if key equals a key already in the tree\n
 *         -ENOMEM if array cannot accomidate the given key.
 */
int
zcore_avlarr_insert(struct zcore_avlarr * the,
                    void const * key,
                    void const * value);

/**
 * Find a node corresponding to the given key
 *
 * @param  the zcore_avlarr in which to lookup key
 * @param  key
 *           Key used to compare the value-nodes. If no cmp function
 *           was provided the key is interpreted as an unsigned word rather
 *           than as a pointer to memory.
 * @return  pointer to value-node.\n NULL on failure.
 */
attr_pure void *
zcore_avlarr_lookup(struct zcore_avlarr const * the, void const * key);

/**
 * Find idx of node corresponding to the given key.
 *
 * @param  the zcore_avlarr in which to lookup key
 * @param  key
 *           Key used to compare the value-nodes. If no cmp function
 *           was provided the key is interpreted as an unsigned word rather
 *           than as a pointer to memory.
 * @param  key_ref
 *           pointer to output key pointer, to write the containers copy of key.
 * @return  pointer to value-node.\n NULL on failure.
 */
attr_pure size_t
zcore_avlarr_lookup_idx(struct zcore_avlarr const * the, void const * key);


/**
 * Remove a node corresponding to the given key
 *
 * @param  key
 *           Key used to compare the value-nodes. If no cmp function
 *           was provided the key is interpreted as an unsigned word rather
 *           than as a pointer to memory.
 * @return pointer to value-node
 */
void *
zcore_avlarr_remove(struct zcore_avlarr * the, void const * key);

/**
 * Remove a node corresponding to the given key
 * Since the key pointer held by the container may point to a different
 * instance of the key, and it may be useful to examine this value, the key_ref
 * parameter is added to this variant of lookup. This also allows the
 * verification of success when there are no values.
 *
 * @param  key
 *           Key used to compare the value-nodes. If no cmp function
 *           was provided the key is interpreted as an unsigned word rather
 *           than as a pointer to memory.
 * @param  key_ref
 *           pointer to output key pointer, to write the containers copy of key.
 * @return pointer to value-node
 */
void *
zcore_avlarr_remove_key_ref(struct zcore_avlarr * avl, void const * key,
                            void ** key_ref);

/**
 * Remove a node corresponding to the given idx.
 *
 * @param the avlarr being modified.
 * @param idx of the item being removed
 * @return the stored value. NULL if idx is invalid.
 */
void *
zcore_avlarr_remove_idx(struct zcore_avlarr * avl, size_t idx);

/**
 * Remove a node corresponding to the given idx.
 *
 * @param the avlarr being modified.
 * @param idx of the item being removed
 * @param key_ref reference to the stored key
 * @return the stored value. NULL if idx is invalid.
 */
void *
zcore_avlarr_remove_idx_key_ref(struct zcore_avlarr * avl, size_t idx,
                                void ** key_ref);

/**
 * Height of the tree
 *
 * @param  the zcore_avlarr for which to asses the height.
 * @return height of the tree
 */
attr_pure int8_t
zcore_avlarr_height(struct zcore_avlarr const * the);

/**
 * Balance of the tree
 *
 * @param  the zcore_avlarr for which to asses the balance.
 * @return balance of the tree. Less than zero for left leaning,
 *         greater than 0 for right leaning.
 */
attr_pure int
zcore_avlarr_balance(struct zcore_avlarr const * the);

/**
 * Optimizes zcore_avlarr.
 * For a BST, the optimal topography is to have a complete binary tree.
 * Therefore, ensure the zcore_avlarr exhibits the binary heap property.
 * That is: restructure the tree so that every level, except possibly the
 * last, is completely filled, and all nodes in the last level are as far
 * left as possible. The product is then a nearly complete binary tree.
 *
 * Useful after major tree is constructed and will not have many new
 * entries or removals.
 *
 * WARNING: Operation has space complexity O(n)
 *
 * @param  the zcore_avlarr to optimize
 * @param  trim
 *           Trim down the capacity to the minimum size necessary
 *           to hold the newly optimized tree. This is helpful if no more
 *           insertions or deletions will be performed for some time.
 * @return
 *         Returns 0 upon success.
 *         Returns -ENOMEM upon failure.
 */
int
zcore_avlarr_optimize(struct zcore_avlarr * the, bool trim);

/**
 * Obtain the index for the first node in compare order.
 * Index indexes the keys and values arrays.
 *
 * @param the avlarr to read
 * @return the first ordered nodes index
 *         SIZE_MAX if list is empty
 */
attr_pure size_t
zcore_avlarr_first_ordered_idx(struct zcore_avlarr const * the);

/**
 * Obtain the index for the last node in compare order.
 * Index indexes the keys and values arrays.
 *
 * @param the avlarr to read
 * @return the last ordered nodes index
 *         SIZE_MAX if list is empty
 */
attr_pure size_t
zcore_avlarr_last_ordered_idx(struct zcore_avlarr const * the);

/**
 * Obtain the previous ordered node in compare order.
 *
 * @param the avlarr to be read
 * @param idx to decrement
 * @return the previous ordered nodes idx
 */
attr_pure size_t
zcore_avlarr_previous_ordered_idx(struct zcore_avlarr const * the, size_t idx);

/**
 * Obtain the next ordered node in compare order.
 *
 * @param the avlarr to be read
 * @param idx to increment
 * @return the next ordered nodes idx
 */
attr_pure size_t
zcore_avlarr_next_ordered_idx(struct zcore_avlarr const * avl, size_t idx);

/**
 * Creates new sorted array of elements contained in the avlarr.
 *
 * @param avl the avlarr to be read
 * @param keys pointer to the array pointer
 * @param values pointer to the array pointer
 * @parma mi memory interface with which to allocate the new arrays.
 * @return length of the allocated arrays.
 */
size_t
zcore_avlarr_create_array(struct zcore_avlarr const * avl,
                          void *** keys, void *** values,
                          struct zcore_memoryif const * mi);


#ifdef __cplusplus
}
#endif

#endif/*ZCORE_AVL_ARRAY_H*/
