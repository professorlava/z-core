#ifndef ZCORE_CONTAINERS_IFWLIST_H
#define ZCORE_CONTAINERS_IFWLIST_H

#include "zcore/stdinc.h"
#include <assert.h>

#define ZCORE_LIST_POISON1 ((void *)0x00100100)
#define ZCORE_LIST_POISON2 ((void *)0x00200200)

struct zcore_ifwlist_head {
	struct zcore_ifwlist_node * first;
};

struct zcore_ifwlist_node {
	struct zcore_ifwlist_node * next;
	struct zcore_ifwlist_node ** pprev;
};

#define ZCORE_IFWLIST_INIT() (struct zcore_ifwlist_head){ NULL }

#define ZCORE_IFWLIST_DEFINE(name) \
	struct zcore_ifwlist_head name = ZCORE_ILIST_INIT()

#define ZCORE_IFWLIST_DECLARE(name) struct zcore_ifwlist_head name

static attr_inline void
zcore_ifwlist_initialize(struct zcore_ifwlist_head * list)
{
	list->first = NULL;
}

static attr_inline void
zcore_ifwlist_node_initialize(struct zcore_ifwlist_node * n)
{
	n->next = NULL;
	n->pprev = NULL;
}

static attr_inline bool
zcore_ifwlist_node_is_orphan(struct zcore_ifwlist_node * n)
{
	return NULL == n->pprev;
}

static attr_inline bool
zcore_ifwlist_is_empty(struct zcore_ifwlist_head * head)
{
	return NULL == head->first;
}

static attr_inline void
zcore_ifwlist_push(struct zcore_ifwlist_head * restrict head,
                   struct zcore_ifwlist_node * restrict new)
{
	new->next = head->first;
	if (head->first)
		head->first->pprev = &new->next;
	head->first = new;
	new->pprev = &head->first;
}

static attr_inline void
zcore_ifwlist_insert_before(struct zcore_ifwlist_node * restrict here,
                            struct zcore_ifwlist_node * restrict new)
{
	new->pprev = here->pprev;
	new->next = here;
	here->pprev = &new->next;
	*(new->pprev) = new;
}

static attr_inline void
zcore_ifwlist_insert_behind(struct zcore_ifwlist_node * restrict here,
                            struct zcore_ifwlist_node * restrict new)
{
	new->next = here->next;
	here->next = new;
	new->pprev = &here->next;
	if (new->next)
		new->next->pprev = &new->next;
}

static attr_inline void
zcore_ifwlist_node_fake(struct zcore_ifwlist_node * node)
{
	node->pprev = &node->next;
}

static attr_inline void
_zcore_ifwlist_extract(struct zcore_ifwlist_node * n)
{
	*(n->pprev) = n->next;
	if (n->next)
		n->next->pprev = n->pprev;
}

static attr_inline  void
_zcore_ifwlist_poison(struct zcore_ifwlist_node * n)
{
	n->next = ZCORE_LIST_POISON1;
	n->pprev = ZCORE_LIST_POISON2;
}

static attr_inline void
zcore_ifwlist_remove(struct zcore_ifwlist_node * n)
{
	_zcore_ifwlist_extract(n);
	_zcore_ifwlist_poison(n);
}

static attr_inline void
zcore_ifwlist_remove_clean(struct zcore_ifwlist_node * n)
{
	if (!zcore_ifwlist_node_is_orphan(n)) {
		_zcore_ifwlist_extract(n);
		zcore_ifwlist_node_initialize(n);
	}
}

/**
 * cut a list into two
 *
 * This helper moves the initial part of src_head, up to and including
 * cut_here, from src_head to dst_head. You should pass on cut_here an
 * element you know is on src_head, *or null*. If cut_here is null,
 * all of src_head is copied to dst_src, as if cut_here was the last
 * element on the list. dst_head should be an empty list or a list
 * you do not care about losing its data.
 */
static attr_inline void
zcore_ifwlist_cut(struct zcore_ifwlist_head * restrict dst,
                  struct zcore_ifwlist_head * restrict src,
                  struct zcore_ifwlist_node * restrict cut_here)
{
	dst->first = src->first;
	if (dst->first)
		dst->first->pprev = &dst->first;
	if (cut_here)
		src->first = cut_here->next;
	else
		src->first = NULL;
	if (src->first)
		src->first->pprev = &src->first;
}

/* TODO Splice */

/**
 * iterate over a forward list
 *
 * @param head of the ifwlist
 * @param pos (struct zcore_ifwlist_node *) to use as loop cursor
 */
#define zcore_ifwlist_foreach(head, pos) \
	for (pos = (head)->first; NULL != pos; pos = (pos)->next)

/**
 * iterate over a forward list, safe against removal of list entry
 *
 * @param head of the ifwlist
 * @param pos (struct zcore_ifwlist_node *) to use as loop cursor
 * @param tmp another (struct zcore_ifwlist_node *) to use as storage
 */
#define zcore_ifwlist_foreach_safe(head, pos, tmp) \
	for (pos = (head)->first; \
	     pos && ({ tmp = (pos)->next; 1; }); \
	     pos = tmp)

/**
 * continue iterating over a forward list
 *
 * @param pos (struct zcore_ifwlist_node *) to use as loop cursor
 */
#define zcore_ifwlist_foreach_continue(pos) \
	for (pos = (pos)->next; NULL != pos; pos = (pos)->next)

/**
 * continue iterating over a forward list, safe against removal of list entry
 *
 * @param pos (struct zcore_ifwlist_node *) to use as loop cursor
 * @param tmp another (struct zcore_ifwlist_node *) to use as storage
 */
#define zcore_ifwlist_foreach_safe_continue(pos, tmp) \
	for (pos = (pos)->next; \
	     pos && ({ tmp = (pos)->next; 1; }); \
	     pos = tmp)

/**
 * get the struct for this entry
 *
 * @param ptr the (struct zcore_ilist_head *)
 * @param the type of the struct this is embedded in
 * @param member the name of the zcore_ilist_head within the struct
 * @return type * ptr, or NULL if ptr is NULL
 */
#define zcore_ifwlist_entry(ptr, type, member) \
	zcore_containerof_safe(ptr, type, member)

/**
 * get first element from a list head
 *
 * @param head of the list
 * @param type of struct list is embedded in
 * @param member of the list head within the struct
 * @return type * to first entry, or NULL if list is empty
 */
#define zcore_ifwlist_first_entry(head, type, member) \
	zcore_ifwlist_entry((head)->first, type, member)

/**
 * get next entry from a list
 *
 * @param node a (type *) to cursor
 * @param member of the list head within the struct
 */
#define zcore_ifwlist_next_entry(node, member) \
	zcore_ifwlist_entry((node)->member.next, typeof(*(node)), member)

/**
 * iterate over forward list of given type
 *
 * @param head of the list
 * @param pos (type *) to use as loop cursor
 * @param member name of the zcore_ifwlist_node within type
 */
#define zcore_ifwlist_foreach_entry(head, pos, member) \
	for (pos = zcore_ifwlist_first_entry(head, typeof(*(pos)), member); \
	     pos; \
	     pos = zcore_ifwlist_next_entry(pos, member))

/**
 * continue iterating over forward list of given type
 *
 * @param pos (type *) to use as loop cursor
 * @param member name of the zcore_ifwlist_node within type
 */
#define zcore_ifwlist_foreach_entry_continue(pos, member) \
	for (pos = zcore_ifwlist_next_entry(pos, member); \
	     pos; \
	     pos = zcore_ifwlist_next_entry(pos, member))

/**
 * iterating over forward list, safe against removeal
 *
 * @param head of the list
 * @param pos (type *) to use as loop cursor
 * @param another (type *) to use as storage
 * @param member name of the zcore_ifwlist_node within type
 */
#define zcore_ifwlist_foreach_entry_safe(head, pos, tmp, member) \
	for (pos = zcore_ifwlist_first_entry(head, typeof(*(pos)), member); \
	     pos && ({ tmp = zcore_ifwlist_next_entry(pos, member); 1; }); \
	     pos = tmp)

/**
* continue iterating over forward list, safe against removeal
*
* @param head of the list
* @param pos (type *) to use as loop cursor
* @param another (type *) to use as storage
* @param member name of the zcore_ifwlist_node within type
*/
#define zcore_ifwlist_foreach_entry_safe_continue(pos, tmp) \
	for (pos = zcore_ifwlist_next_entry(pos, member), \
	     pos && ({ tmp = zcore_ifwlist_next_entry(pos, member); 1; }); \
	     pos = tmp) \

#endif/*ZCORE_CONTAINERS_IFWLIST_H*/
