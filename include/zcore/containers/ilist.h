#ifndef ZCORE_CONTAINERS_ILIST_H
#define ZCORE_CONTAINERS_ILIST_H

#include "zcore/stdinc.h"

#define ZCORE_LIST_POISON1 ((void *)0x00100100)
#define ZCORE_LIST_POISON2 ((void *)0x00200200)

struct zcore_ilist_head {
	struct zcore_ilist_head * next;
	struct zcore_ilist_head * prev;
};

#define ZCORE_ILIST_INIT(name) (struct zcore_ilist_head){ &(name), &(name) }

#define ZCORE_ILIST_DEFINE(name) \
	struct zcore_ilist_head name = ZCORE_ILIST_INIT(name)

#define ZCORE_ILIST_DECLARE(name) struct zcore_ilist_head name

static attr_inline void
zcore_ilist_initialize(struct zcore_ilist_head * list)
{
	list->next = list;
	list->prev = list;
}

/**
 * Ensure two list entries are inter-linked.
 */
static attr_inline void
_zcore_ilist_link(struct zcore_ilist_head * one,
                  struct zcore_ilist_head * two)
{
	one->next = two;
	two->prev = one; /* can this be cache optimized? */
}

/*
 * Insert a new entry between two known consecutive entries.
 *
 * This is only for internal list manipulation where we know
 * the prev/next entries already!
 */
static attr_inline void
_zcore_ilist_inject(struct zcore_ilist_head * prev,
                    struct zcore_ilist_head * new,
                    struct zcore_ilist_head * next)
{
	// XXX does order matter?
	_zcore_ilist_link(new, next);
	_zcore_ilist_link(prev, new);
}

/**
 * Insert a new entry after the specified head.
 * This places the entry at the beginning of the list.
 * This is good for implementing stacks.
 */
static attr_inline void
zcore_ilist_push(struct zcore_ilist_head * head,
                 struct zcore_ilist_head * new)
{
	_zcore_ilist_inject(head, new, head->next);
}

/**
 * Insert a new entry before the specified head.
 * This places the entry at the end of the list.
 * This is useful for implementing queues.
 */
static attr_inline void
zcore_ilist_append(struct zcore_ilist_head * head,
                   struct zcore_ilist_head * new)
{
	_zcore_ilist_inject(head->prev, new, head);
}

/**
 * Remove entry from list.
 */
static attr_inline void
_zcore_ilist_extract(struct zcore_ilist_head * entry)
{
	_zcore_ilist_link(entry->prev, entry->next);
}

/**
 * Poison an list head
 */
static attr_inline void
_zcore_ilist_poison(struct zcore_ilist_head * entry)
{
	entry->next = ZCORE_LIST_POISON1;
	entry->prev = ZCORE_LIST_POISON2;
}

/**
 * Remove entry from list.
 * Note: list_empty() on entry does not return true after this, the entry is
 * in an undefined state.
 */
static attr_inline void
zcore_ilist_remove(struct zcore_ilist_head * entry)
{
	_zcore_ilist_extract(entry);
	_zcore_ilist_poison(entry);
}

/**
 * Remove entry from list and reinitialize.
 */
static attr_inline void
zcore_ilist_remove_clean(struct zcore_ilist_head * entry)
{
	_zcore_ilist_extract(entry);
	zcore_ilist_initialize(entry);
}

/**
 * Replace old entry by new one
 *
 * NOTE:
 * If old was empty, it will be modified and left in an odd state:
 *
 *     new->prev == new
 *     new->next == new
 *     old->prev == new
 *     old->next == old
 *
 * Old will return empty()==false, and it will not be in a valid state.
 * New will return empty()==true and be in a valid state.
 * So it should probably not be called root heads.
 */
static attr_inline void
zcore_ilist_replace(struct zcore_ilist_head * old,
                    struct zcore_ilist_head * new)
{
	_zcore_ilist_inject(old->prev, new, old->next);
	_zcore_ilist_poison(old);
}

/**
 * Replace old entry by new one, and reinitialize old one.
 */
static attr_inline void
zcore_ilist_replace_clean(struct zcore_ilist_head * old,
                          struct zcore_ilist_head * new)
{
	_zcore_ilist_inject(old->prev, new, old->next);
	zcore_ilist_initialize(old);
}

/**
 * Delete entry from one list and add as another's head
 */
static attr_inline void
zcore_ilist_move(struct zcore_ilist_head * head,
                 struct zcore_ilist_head * entry)
{
	_zcore_ilist_extract(entry);
	zcore_ilist_push(head, entry);
}

/**
 * Tests wheter an entry is last in the list represented by head.
 */
static attr_inline bool
zcore_ilist_is_last(struct zcore_ilist_head const * head,
                    struct zcore_ilist_head const * entry)
{
	return (bool)(entry->next == head);
}

/**
 * Tests wheter a list is empty.
 */
static attr_inline bool
zcore_ilist_is_empty(struct zcore_ilist_head const * head)
{
	return (bool)(head->next == head);
}

/**
 * Tests whether a list is empty and not being modified.
 *
 * Description:
 * tests whether a list is empty *and* checks that no other CPU might be
 * in the process of modifying either member (next or prev)
 *
 * NOTE: using empty_careful without synchronization can only be safe if
 * the only activity that can happen to the list entry is
 * zcore_ilist_remove_clean(). Eg. it cannot be used if another CPU
 * could re-push or re-append it.
 */
static attr_inline bool
zcore_ilist_is_empty_careful(struct zcore_ilist_head const * head)
{
        struct zcore_ilist_head * next = head->next;
        return (bool)((next == head) && (next == head->prev));
}

/**
 * Test whether a list is just one entry.
 */
static attr_inline bool
zcore_ilist_is_singular(struct zcore_ilist_head const * head)
{
	return !zcore_ilist_is_empty(head) && (head->next == head->prev);
}

/**
 * Rotate the list to the left
 */
static attr_inline void
zcore_ilist_rotate_forward(struct zcore_ilist_head * head)
{
	if (!zcore_ilist_is_empty(head)) {
		zcore_ilist_move(head->prev, head->next);
	}
}

/**
 * Rotate the list to the right
 */
static attr_inline void
zcore_ilist_rotate_backward(struct zcore_ilist_head * head)
{
	if (!zcore_ilist_is_empty(head)) {
		zcore_ilist_move(head->next, head->prev);
	}
}

/**
 * cut a list into two
 *
 * This helper moves the initial part of src_head, up to and
 * including cut_here, from src_head to dst_head. You should
 * pass on cut_here an element you know is on src_head. dst_head
 * should be an empty list or a list you do not care about
 * losing its data.
 */
static attr_inline void
zcore_ilist_cut(struct zcore_ilist_head * dst_head,
                struct zcore_ilist_head * src_head,
                struct zcore_ilist_head * cut_here)
{
#if 0
	if (zcore_ilist_is_empty(src_head))
	 || (zcore_ilist_is_singular(src_head)
	     && (src_head->next != cut_here && src_head != cut_here))
	     return;
#endif
	if (src_head == cut_here) {
	 	zcore_ilist_initialize(dst_head);
	} else {
		_zcore_ilist_link(dst_head, src_head->next);
		_zcore_ilist_link(src_head, cut_here->next);
		_zcore_ilist_link(cut_here, dst_head);
	}
}

/*
 * Insert a new entry between two known consecutive entries.
 *
 * This is only for internal list manipulation where we know
 * the prev/next entries already!
 */
static attr_inline void
_zcore_ilist_splice(struct zcore_ilist_head * prev,
                    struct zcore_ilist_head * first,
                    struct zcore_ilist_head * last,
                    struct zcore_ilist_head * next)
{
	_zcore_ilist_link(last, next);
	_zcore_ilist_link(prev, first);
}

/**
 * join two lists
 */
static attr_inline void
zcore_ilist_splice(struct zcore_ilist_head * dst,
                   struct zcore_ilist_head * src)
{
	if (!zcore_ilist_is_empty(src))
		_zcore_ilist_splice(dst, src->next, src->prev, dst->next);
}

/**
 * join two lists and reinitialise the emptied list.
 */
static attr_inline void
zcore_ilist_splice_clean(struct zcore_ilist_head * dst,
                         struct zcore_ilist_head * src)
{
	if (!zcore_ilist_is_empty(src)) {
		_zcore_ilist_splice(dst, src->next, src->prev, dst->next);
		zcore_ilist_initialize(src);
	}
}

/**
 * iterate over a list
 *
 * @param head of the list
 * @param pos (struct zcore_ilist_head *) to use as loop cursor
 */
#define zcore_ilist_foreach(head, pos) \
	for (pos = (head)->next; pos != (head); pos = (pos)->next)

/**
 * iterate over a list backwards
 *
 * @param head of the list
 * @param pos (struct zcore_ilist_head *) to use as loop cursor
 */
#define zcore_ilist_foreach_prev(head, pos) \
	for (pos = (head)->prev; pos != (head); pos = (pos)->prev)

/**
 * continue iterate over a list
 *
 * @param head of the list
 * @param pos (struct zcore_ilist_head *) to use as loop cursor
 */
#define zcore_ilist_foreach_continue(head, pos) \
	for (pos = (pos)->next; pos != (head); pos = (pos)->next)

/**
 * continue iterate over a list backwards
 *
 * @param head of the list
 * @param pos (struct zcore_ilist_head *) to use as loop cursor
 */
#define zcore_ilist_foreach_prev_continue(head, pos) \
	for (pos = (pos)->prev; pos != (head); pos = (pos)->prev)

/**
 * iterate over a list safe against removal of list entry
 *
 * @param head of the list
 * @param pos (struct zcore_ilist_head *) to use as loop cursor
 * @param tmp another (struct zcore_ilist_head *) to use as storage
 */
#define zcore_ilist_foreach_safe(head, pos, tmp) \
	for (pos = (head)->next, tmp = (pos)->next; pos != (head); \
	     pos = tmp, tmp = (pos)->next)

/**
 * iterate over a list backwards safe against removal of list entry
 * @param head of the list
 * @param pos (struct zcore_ilist_head *)to use as loop cursor
 * @param tmp another (struct zcore_ilist_head *) to use as storage
 */
#define zcore_ilist_foreach_prev_safe(head, pos, tmp) \
	for (pos = (head)->prev, tmp = (pos)->prev; pos != (head); \
	     pos = tmp, tmp = (pos)->prev)

/**
 * continue iterate over a list safe against removal of list entry
 *
 * @param head of the list
 * @param pos (struct zcore_ilist_head *) to use as loop cursor
 * @param tmp another (struct zcore_ilist_head *) to use as storage
 */
#define zcore_ilist_foreach_safe_continue(head, pos, tmp) \
	for (pos = (pos)->next, tmp = (pos)->next; pos != (head); \
	     pos = tmp, tmp = (pos)->next)

/**
 * continue iterate over a list backwards safe against removal of list entry
 *
 * @param head of the list
 * @param pos (struct zcore_ilist_head *) to use as loop cursor
 * @param tmp another (struct zcore_ilist_head *) to use as storage
 */
#define zcore_ilist_foreach_prev_safe_continue(head, pos, tmp) \
	for (pos = (pos)->prev, tmp = (pos)->prev; pos != (head); \
	     pos = tmp, tmp = (pos)->prev)

/**
 * Get the (type *) struct for this entry
 *
 * @param ptr (struct zcore_ilist_head *) for entry
 * @param type of struct list is embedded in
 * @param member name of the list within the struct
 * @return entry
 */
#define zcore_ilist_entry(ptr, type, member) \
	zcore_containerof(ptr, type, member)

/**
 * Get first entry from a list
 *
 * @param head of the list
 * @param type of struct list is embedded in
 * @param member name of the list within the struct
 * @return first entry
 */
#define zcore_ilist_first_entry(head, type, member) \
	zcore_ilist_entry((head)->next, type, member)

/**
 * Get last entry from a list
 *
 * @param head of the list
 * @param type of struct list is embedded in
 * @param member name of the list within the struct
 * @return last entry
 */
#define zcore_ilist_last_entry(head, type, member) \
	zcore_ilist_entry((head)->prev, type, member)

/**
 * Get next entry from a list
 *
 * @param pos a (type *) of the current entry
 * @param member name of the list within type
 * @return next entry
 */
#define zcore_ilist_next_entry(pos, member) \
	zcore_ilist_entry((pos)->member.next, typeof(*(pos)), member)

/**
 * Get prev entry from a list
 *
 * @param pos a (type *) of the current entry
 * @param member name of the list within type
 * @return next entry
 */
#define zcore_ilist_prev_entry(pos, member) \
	zcore_ilist_entry((pos)->member.prev, typeof(*(pos)), member)

/**
 * iterate over list of given type
 *
 * @param head of the list
 * @param pos a (type *) to use as loop cursor
 * @param member name of the zcore_ilist_head within type.
 */
#define zcore_ilist_foreach_entry(head, pos, member) \
	for (pos = zcore_ilist_first_entry(head, typeof(*(pos)), member); \
	     &(pos)->member != (head); \
	     pos = zcore_ilist_next_entry(pos, member))

/**
 * iterate backwards over list of given type
 *
 * @param head of the list
 * @param pos (type *) to use as loop cursor
 * @param member name of the zcore_ilist_head within type.
 */
#define zcore_ilist_foreach_prev_entry(head, pos, member) \
	for (pos = zcore_ilist_last_entry(head, typeof(*(pos)), member); \
	     &(pos)->member != (head); \
	     pos = zcore_ilist_prev_entry(pos, member))

/**
 * Continue iteration over a list of given type
 *
 * @param head of the list
 * @param pos (type *) to use as loop cursor
 * @param member name of the zcore_ilist_head within type.
 */
#define zcore_ilist_foreach_entry_continue(head, pos, member) \
	for (pos = zcore_ilist_next_entry(pos, member); \
	     &(pos)->member != (head); \
	     pos = zcore_ilist_next_entry(pos, member))

/**
 * Continue iterate backwards over list of given type
 *
 * @param head of the list
 * @param pos (type *) to use as loop cursor
 * @param member name of the zcore_ilist_head within type.
 */
#define zcore_ilist_foreach_prev_entry_continue(head, pos, member) \
	for (pos = zcore_ilist_prev_entry(pos, member); \
	     &(pos)->member != (head); \
	     pos = zcore_ilist_prev_entry(pos, member))

/**
 * iterate over list of given type safe against removal
 *
 * @param head of the list
 * @param pos (type *) to use as loop cursor
 * @param tmp another (type *) to use as temporary storage.
 * @param member name of the zcore_ilist_head within type.
 */
#define zcore_ilist_foreach_entry_safe(head, pos, tmp, member) \
	for (pos = zcore_ilist_first_entry(head, typeof(*(pos)), member); \
	     &(pos)->member != (head) \
	     && ({ tmp = zcore_ilist_next_entry(pos, member); 1;}); \
	     pos = tmp)

/**
 * iterate backwards over list of given type safe against removal
 *
 * @param head of the list
 * @param pos (type *) to use as loop cursor
 * @param tmp another (type *) to use as temporary storage.
 * @param member name of the zcore_ilist_head within type.
 */
#define zcore_ilist_foreach_prev_entry_safe(head, pos, tmp, member) \
	for (pos = zcore_ilist_last_entry(head, typeof(*(pos)), member); \
	     &(pos)->member != (head) \
	     && ({ tmp = zcore_ilist_prev_entry(pos, member); 1; }); \
	     pos = tmp)

/**
 * Continue iterate over list of given type safe against removal
 *
 * @param head of the list
 * @param pos (type *) to use as loop cursor
 * @param tmp another (type *) to use as temporary storage.
 * @param member name of the zcore_ilist_head within type.
 */
#define zcore_ilist_foreach_entry_safe_continue(head, pos, tmp, member) \
	for (pos = zcore_ilist_next_entry(pos, member); \
	     &(pos)->member != (head) \
	     && ({ tmp = zcore_ilist_next_entry(pos, member); 1; }); \
	     pos = tmp)

/**
 * Continue iterate backwards over list of given type safe against removal
 *
 * @param head of the list
 * @param pos (type *) to use as loop cursor
 * @param tmp another (type *) to use as temporary storage.
 * @param member name of the zcore_ilist_head within type.
 */
#define zcore_ilist_foreach_prev_entry_safe_continue(head, pos, tmp, member) \
	for (pos = zcore_ilist_prev_entry(pos, member); \
	     &(pos)->member != (head) \
	     && ({ tmp = zcore_ilist_prev_entry(pos, member); 1; }); \
	     pos = tmp)

#endif/*ZCORE_CONTAINERS_ILIST_H*/
