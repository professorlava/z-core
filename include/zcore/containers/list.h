#ifndef ZCORE_CONTAINERS_LIST_H
#define ZCORE_CONTAINERS_LIST_H

#include "zcore/stdinc.h"
#include "zcore/memory.h"
#include "zcore/destructor.h"
#include "zcore/containers/ilist.h"

#ifdef __cplusplus
extern "C" {
#endif

struct zcore_list_node {
	void * item;
	struct zcore_ilist_head list;
};

struct zcore_list {
	struct zcore_ilist_head list; /**< internal list **/
	size_t length; /**< Number of items pairs currently held */
	struct zcore_destructor const * dtor;
	struct zcore_memoryif const * mi;
};

extern struct zcore_destructor const * const zcore_list_destructor;

/**
 * Initialize a zcore_list.
 *
 * @param  the zcore_list to construct.
 * @param  dtor
 *           Fn to handle the transfer of ownership of the value.
 *           This can mean either freeing allocated memory that is no
 *           longer needed, or nominally ensuring the memory is still
 *           reachable elsewhere.
 * @param  mi
 *           Memory Interface used to dynamically allocate nodes.
 *           If NULL is given, stdmi is used.
 * @return 0 on success\n
 *         -EINVAL if any input is invalid or ill formed\n
 */
void
zcore_list_initialize(struct zcore_list * the,
                      struct zcore_destructor const * dtor,
                      struct zcore_memoryif const * mi);

/**
 * Destruct a list
 * First applying passed dtor to every remaining element in the list.
 *
 * @param  the zcore_list to destuct
 */
void
zcore_list_destruct(struct zcore_list * the);

/**
 * Clears the internal storage of the container.
 * Does not call the destructor on held items, nor destructs the container
 *
 * @param the list to flush
 */
void
zcore_list_flush(struct zcore_list * the);

/**
 * Insert an item at a given position.
 * The first argument is the index of the element before which to insert,
 *
 * @param the list to insert into.
 * @param item to insert into the list
 * @return 0 on success\n
 *         -ENOMEM if item cannot be added to the list.
 */
int
zcore_list_insert(struct zcore_list * the, size_t idx, void * item);

/**
 * Get item at idx in list.
 *
 * @param the list to get item from
 * @param idx to get
 * @return pointer to the item
 */
void *
zcore_list_get(struct zcore_list const * the, size_t idx);

/**
 * Remove item at idx.
 *
 * @param the list to remove item from
 * @param item to remove from the list
 * @return pointer to the item after removal
 */
void *
zcore_list_remove(struct zcore_list * the, void * item);

/**
 * Remove item at idx.
 *
 * @param the list to remove item from
 * @param item to remove from the list
 * @return pointer to the item after removal
 */
void *
zcore_list_remove_idx(struct zcore_list * the, size_t idx);

/**
 * Remove and destroy first occurrence of item.
 *
 * @param the list to remove item from
 * @param item to remove from the list
 * @return 0 on success\n
 *         -EINVAL if item is not in list
 */
int
zcore_list_delete(struct zcore_list * the, void * item);

/**
 * Remove and destroy first occurrence of item.
 *
 * @param the list to remove item from
 * @param item to remove from the list
 * @return 0 on success\n
 *         -EINVAL if idx is invalid
 */
int
zcore_list_delete_idx(struct zcore_list * the, size_t idx);

/**
 * Get index of first occurrence of item.
 *
 * @param the list to remove item from
 * @param item to remove from the list
 * @return SIZE_MAX on failure\n
 *         idx of item otherwise
 */
size_t
zcore_list_indexof(struct zcore_list * the, void * item);

/**
 * Return the number of times x appears in the list.
 *
 * @param the list to read
 * @param item to tally
 * @return tally of occurrences of item
 */
size_t
zcore_list_countof(struct zcore_list * the, void * item);

/**
 * Push item to the front of the list
 *
 * @param the list to modify
 * @param item to add
 * @return 0 on success\n
 *         -ENOMEM if item cannot be added to the list.
 */
int
zcore_list_push_front(struct zcore_list * the, void const * item);

/**
 * Append item to the back of the list
 *
 * @param the list to modify
 * @param item to add
 * @return 0 on success\n
 *         -ENOMEM if item cannot be added to the list.
 */
int
zcore_list_push_back(struct zcore_list * the, void const * item);

/**
 * Remove item from the front of the list
 *
 * @param the list to modify
 * @return the item that was at the front of the list
 */
void *
zcore_list_pop_front(struct zcore_list * the);

/**
 * Remove item from the back of the list
 *
 * @param the list to modify
 * @return the item that was at the back of the list
 */
void *
zcore_list_pop_back(struct zcore_list * the);

#ifdef __cplusplus
}
#endif

#endif/*ZCORE_CONTAINERS_LIST_H*/
