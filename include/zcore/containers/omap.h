/** \file
 *
 * Ordered Map implementation
 *
 * Implements Map abstract container using underlying code data container.
 */

#ifndef ZCORE_CONTAINERS_OMAP_H
#define ZCORE_CONTAINERS_OMAP_H

#include "zcore/config.h"
#include "zcore/stdinc.h"
#include "zcore/containers/avlarr.h"

#ifdef __cplusplus
extern "C" {
#endif

struct zcore_omap {
	struct zcore_avlarr avlarr;
	struct zcore_destructor const * key_dtor;
	struct zcore_destructor const * val_dtor;
};

extern struct zcore_destructor const * const zcore_omap_destructor;

/**
 * Construct an ordered map.
 *
 * @param  the zcore_omap to construct.
 * @param  dtor
 *           Fn to handle the transfer of ownership of the key/value pair.
 *           This can mean either freeing allocated memory that is no
 *           longer needed, or nominally ensuring the memory is still
 *           reachable elsewhere.
 * @param  key_cmp Comparator for key types.
 * @param  mi
 *           Memory interface used to dynamically allocate and resize the map.
 *           If NULL is given, stdmi is used as a default.
 * @return 0 on success\n
 *         -EINVAL if any input is invalid or illformed\n
 *         -ENOMEM if map cannot be constructed due to insufficient memory.
 */
static attr_inline int
zcore_omap_construct(struct zcore_omap * the,
                     struct zcore_destructor const * key_dtor,
                     struct zcore_destructor const * val_dtor,
                     struct zcore_comparer const * key_cmp,
                     struct zcore_memoryif const * mi)
{
	the->key_dtor = key_dtor;
	the->val_dtor = val_dtor;
	return zcore_avlarr_construct(&the->avlarr, true,
	                              ZCORE_OMAP_INITIAL_HEIGHT, key_cmp, mi);
}

/**
 * Destruct an ordered map.
 * First applying passed dtor to every remaining element in the map.
 *
 * @param  the zcore_omap to destuct

 */
static attr_inline void
zcore_omap_destruct(struct zcore_omap * the)
{
	zcore_avlarr_destruct(&the->avlarr, the->key_dtor, the->val_dtor);
}

/**
 * Clear the internal storage of the container.
 * This does not call destructors on held items, or destruct the container.
 *
 * @param the omap to flush
 */
static attr_inline void
zcore_omap_flush(struct zcore_omap * the)
{
	zcore_avlarr_flush(&the->avlarr);
}

/**
 * Insert a key/value pair into the map.
 * If the key already exists in the map, it will be replaced with the
 * new key/value. The destructor will be called on the old key/value pair.
 *
 * @param  the zcore_omap to insert into.
 * @param  key key to insert into the map.
 * @param  value value to insert into the map.
 * @return 0 on success\n
 *         -EINVAL if key is null\n
 *         -ENOMEM if there is not enough memory to fit the element.
 */
int
zcore_omap_insert(struct zcore_omap * the, void const * key,
                  void const * value);

/**
 * Get the value stored for a given key.
 *
 * @param  the zcore_omap to inspect
 * @param  key
 *           Key used to compare the value-nodes. If no cmp function
 *           was provided the key is interpreted as an unsigned word rather
 *           than as a pointer to memory.
 * @return  pointer to value-node.\n NULL on failure.
 */
static attr_inline void *
zcore_omap_get(struct zcore_omap const * the, void const * key)
{
	return zcore_avlarr_lookup(&the->avlarr, key);
}

/**
 * Checks if an element is contained within the map.
 *
 * @param  the map being examined.
 * @param  key to check belongs to map.
 * @return true, if key is an element of the map.\n
 *         false, if the key is not an element of the map.
 */
static attr_inline bool
zcore_omap_contains(struct zcore_omap const * the, void const * key)
{
	if (SIZE_MAX != zcore_avlarr_lookup_idx(&the->avlarr, key))
		return true;
	else
		return false;
}

/**
 * Remove a value from the map.
 *
 * @param  the map being modified.
 * @param  key to remove from the set.
 * @return pointer to the value removed from the set.
 */
static attr_inline void *
zcore_omap_remove(struct zcore_omap * the, void const * key)
{
	return zcore_avlarr_remove(&the->avlarr, key);
}

/**
 * Remove a key/value pair from the map.
 *
 * @param  the map being modified.
 * @param  key to remove from the set.
 * @param  key_ref reference to a key pointer to save the internally stored key.
 * @return pointer to the value removed from the set.
 */
static attr_inline void *
zcore_omap_remove_key_ref(struct zcore_omap * the, void const * key,
                          void ** key_ref)
{
	return zcore_avlarr_remove_key_ref(&the->avlarr, key, key_ref);
}

/**
 * Optimizes the zcore_omap
 *
 * WARNING: operation has a space complexity of O(n) while running.
 *
 * @param the zcore_omap to optimize.
 * @param trim
 *          trim down the capacity to the minimum size neccessary to hold
 *          the newly optimized map. This is helpful if no more insertions
 *          or removals will be performed for some time.
 * @return 0 on success\n
 *         -ENOMEM when there isnt enough memory O(n) for the operation.
 */
static attr_inline int
zcore_omap_optimize(struct zcore_omap * the, bool trim)
{
	return zcore_avlarr_optimize(&the->avlarr, trim);
}

/**
 * Delete a key/value pair from the map.
 * Calls the dtor on the key/value pair after removing from the map.
 *
 * @param  the map being modified.
 * @param  key to delete from map
 */
void
zcore_omap_delete(struct zcore_omap * the, void const * key);


/**
 * Number of key/value pairs within the map
 */
static attr_inline size_t
zcore_omap_size(struct zcore_omap const * the)
{
	return the->avlarr.count;
}

/**
 * Creates new sorted array of elements contained in the map.
 *
 * @param avl the avlarr to be read
 * @param keys pointer to the array pointer
 * @param values pointer to the array pointer
 * @parma mi memory interface with which to allocate the new arrays.
 * @return length of the allocated arrays.
 */
static attr_inline size_t
zcore_omap_create_array(struct zcore_omap const * the,
                        void *** keys, void *** values,
                        struct zcore_memoryif const * mi)
{
	return zcore_avlarr_create_array(&the->avlarr, keys, values, mi);
}

#ifdef __cplusplus
}
#endif

#endif/*ZCORE_CONTAINERS_OMAP_H*/
