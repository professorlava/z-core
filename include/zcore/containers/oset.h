/** \file
 *
 * Ordered Set implementation.
 *
 * Implements Set abstract container using underlying
 * core data container.
 */

#ifndef ZCORE_CONTAINERS_OSET_H
#define ZCORE_CONTAINERS_OSET_H

#include "zcore/config.h"
#include "zcore/stdinc.h"
#include "zcore/containers/avlarr.h"

#ifdef __cplusplus
extern "C" {
#endif

struct zcore_oset {
	struct zcore_avlarr avlarr;
	struct zcore_destructor const * dtor;
};

extern struct zcore_destructor const * const zcore_oset_destructor;

/**
 * Construct a Resizing zcore_oset.
 *
 * @param  the zcore_oset to construct.
 * @param  dtor
 *           Fn to handle the transfer of ownership of the element.
 *           This can mean either freeing allocated memory that is no
 *           longer needed, or nominally ensuring the memory is still
 *           reachable elsewhere.
 * @param  key_cmp Comparator for keys type.
 * @param  mi
 *           Memory Interface used to dynamically allocate and resize
 *           elem and height arrays. If NULL is given, stdmi is used.
 * @return 0 on success\n
 *         -EINVAL if any input is invalid or ill formed\n
 *         -ENOMEM if keys, values, or heights arrays failed to allocate.
 */
static attr_inline int
zcore_oset_construct(struct zcore_oset * the,
                     struct zcore_destructor const * dtor,
                     struct zcore_comparer const * key_cmp,
                     struct zcore_memoryif const * mi)
{
	the->dtor = dtor;
	return zcore_avlarr_construct(&the->avlarr, false,
	                              ZCORE_OSET_INITIAL_HEIGHT, key_cmp, mi);
}

/**
 * Destruct an zcore_oset.
 * First applying passed dtor to every remaining element in the set.
 *
 * @param  the zcore_oset to destruct
 */
static attr_inline void
zcore_oset_destruct(struct zcore_oset * the)
{
	zcore_avlarr_destruct(&the->avlarr, the->dtor, NULL);
}

/**
 * Flush the internal storage of the container.
 * This does not call destructors on held items, or destruct the container.
 *
 * @param the oset to flush
 */
static attr_inline void
zcore_oset_flush(struct zcore_oset * the)
{
	zcore_avlarr_flush(&the->avlarr);
}

/**
 * Insert an element into the set.
 *
 * @param  the zcore_oset to insert into
 * @param  elem element to store in the set.
 * @return 0 on success\n
 *         -EINVAL if elem is NULL.\n
 *         -ENOMEM if set cannot accomidate the given elem.
 */
int
zcore_oset_insert(struct zcore_oset * the, void const * elem);

/**
 * Checks if an element is contained within a set.
 *
 * @param the set being examined.
 * @param elem to check belongs to set.
 * @return true, if elem is an element of set.\n
 *         false, if elem is not an element of set.
 */
static attr_inline bool
zcore_oset_contains(struct zcore_oset const * the, void const * elem)
{
	if (zcore_avlarr_lookup_idx(&the->avlarr, elem) != SIZE_MAX)
		return true;
	else
		return false;
}

/**
 * Remove an element from the set.
 *
 * @param  the set being modified.
 * @param  elem element to remove from set.
 * @return pointer to element as inserted, since it may be a different instance
 *         than the key provided, and need cleanup.
 */
static attr_inline void *
zcore_oset_remove(struct zcore_oset * the, void const * elem)
{
	void * elem_ref = NULL;
	zcore_avlarr_remove_key_ref(&the->avlarr, elem, &elem_ref);
	return elem_ref;
}

/**
 * Delete a element from the map.
 * Calls the dtor on the elem after removing from the set.
 *
 * @param  the set being modified.
 * @param  elem to delete from set
 */
void
zcore_oset_delete(struct zcore_oset * the, void const * elem);

/**
 * Optimizes zcore_oset.
 *
 * WARNING: Operation has space complexity O(n) while running.
 *
 * @param  the zcore_oset to optimize
 * @param  trim
 *           Trim down the capacity to the minimum size necessary
 *           to hold the newly optimized tree. This is helpful if no more
 *           insertions or deletions will be performed for some time.
 * @return
 *         Returns 0 upon success.
 *         Returns -ENOMEM upon failure.
 */
static attr_inline int
zcore_oset_optimize(struct zcore_oset * the, bool trim)
{
	return zcore_avlarr_optimize(&the->avlarr, trim);
}

/**
 * The cardinality | S | of a set S is "the number of members of S."
 *
 * @param the set being examined.
 * @return cardinality of the set.
 */
static attr_inline size_t
zcore_oset_size(struct zcore_oset const * the)
{
	return the->avlarr.count;
}

/**
 * Check the subset property/relationship of two sets.
 *
 * @param A set being examined for superset property
 * @param B set to examine as subset of a
 * @return true, if A ⊆ B.\n
 *         false, otherwise.
 */
bool
zcore_oset_is_subset(struct zcore_oset const * A, struct zcore_oset const * B);

/**
 * Check the proper subset property/relationship of two sets.
 *
 * @param A set being examined for superset property
 * @param B set to examine as subset of a
 * @return true, if A ⊆ B, A ≠ B.\n
 *         false, otherwise.
 */
bool
zcore_oset_is_proper_subset(struct zcore_oset const * A,
                            struct zcore_oset const * B);

/**
 * Check if two sets have zero overlapping elements.
 *
 * @param A set to examine.
 * @param B set to examine.
 * @return true, if A and B share no elements.
 */
bool
zcore_oset_is_disjoint(struct zcore_oset const * A,
                       struct zcore_oset const * B);

/**
 * Check if two sets have at least one common element.
 *
 * @param A set to examine
 * @param B set to examine
 * @return true, if A and B share at least one element.
 */
bool
zcore_oset_is_intersecting(struct zcore_oset const * A,
                           struct zcore_oset const * B);

/**
 * Perform Union operation between two sets and store results in the first.
 *
 * The operation can be described with set-notation as follows:
 *
 *     A := A ∪ B
 *
 * @param A set to apply, and store results.
 * @param B set to apply.
 * @return 0 on success.\n
 *         -ENOMEM if A cannot accomidate the full results of the union of
 *         B. A contents are likely clobbered from a partial union.
 */
int
zcore_oset_or(struct zcore_oset * A, struct zcore_oset const * B);
#define zcore_oset_union(...) zcore_oset_or(__VA_ARGS__)

/**
 * Perform Union operation between two sets, and union the results into
 * the third set.
 *
 * The operation can be described with set-notation as follows:
 *
 *     C := C ∪ A ∪ B
 *
 * @param C set to apply and store resulting union.
 * @param A set to apply intersection.
 * @param B set to apply intersection.
 * @return 0 on success.
 *         -ENOMEM if C cannot accomidate the full results of the union of
 *         A and B. C contents are likely clobbered from a partial union.
 */
int
zcore_oset_or_into(struct zcore_oset * C, struct zcore_oset const * A,
                   struct zcore_oset const * B);
#define zcore_oset_union_into(...) zcore_oset_or_into(__VA_ARGS__)

/**
 * Perform Intersection operation between two sets and store results in
 * the first.
 *
 * the dtor given at construction is called on elements dropped from
 * A due to the intersection of A with B.
 *
 * @param A  set to apply, and store results.
 * @param B  set to apply.
 * @return 0 on success.
 *        -ENOMEM if A cannot accomidate the full results of the intersection
 *          of B. A contents are likely clobbered from a partial intersection.
 */
int
zcore_oset_and(struct zcore_oset * A, struct zcore_oset const * B);
#define zcore_oset_intersect(...) zcore_oset_and(__VA_ARGS__)

/**
 * Perform Intersection operation between two sets, and union the results into
 * the third set.
 *
 * The operation can be described with set-notation as follows:
 *
 *     C := C ∪ (A ∩ B)
 *
 * @param C set to apply and store resulting union.
 * @param A set to apply intersection.
 * @param B set to apply intersection.
 * @return 0 on success.
 *         -ENOMEM if C cannot accomidate the full results of the intersection
 *           of A and B. C contents are likely clobbered from a partial op.
 */
int
zcore_oset_and_into(struct zcore_oset * C, struct zcore_oset const * A,
                    struct zcore_oset const * B);
#define zcore_oset_intersect_into(...) zcore_oset_and_into(__VA_ARGS__)

/**
 * Perform Set-Theoretic Difference operation between two sets and store
 * results in third set.
 *
 * The operation can be described with set-notation as follows:
 *
 *     A := A ∖ B
 *
 * @param A set to apply, and store results.
 * @param B set to apply.
 * @return 0 on success.
 *        -ENOMEM if A cannot accomidate the full results of the difference
 *           B. A contents are likely clobbered from a partial difference.
 */
int
zcore_oset_sub(struct zcore_oset * A, struct zcore_oset const * B);
#define zcore_oset_difference(...) zcore_oset_sub(__VA_ARGS__)

/**
 * Perform Set-Theoretic Difference operation between two sets, and union
 * the results into the third set.
 *
 * The operation can be described with set-notation as follows:
 *
 *     C := C ∪ (A ∖ B)
 *
 * @param C set to store resulting union.
 * @param A set to apply.
 * @param B set to apply.
 * @return 0 on success.
 *         -ENOMEM if C cannot accomidate the full results of the difference
 *           of A and B. C contents are likely clobbered from the partial op.
 */
int
zcore_oset_sub_into(struct zcore_oset * C, struct zcore_oset const * A,
                    struct zcore_oset const * B);
#define zcore_oset_difference_into(...) zcore_oset_sub_into(__VA_ARGS__)

/**
 * Perform Symmetric Difference operation between two sets and store
 * results in third set.
 *
 * The operation can be described with set-notation as follows:
 *
 *     A := (A ⊕ B)
 *
 * @param A set to apply, and store results.
 * @param B set to apply.
 * @return 0 on success.
 *        -ENOMEM if A cannot accomidate the full results of the xor of
 *         B. A contents are likely clobbered from a partial op.

 */
int
zcore_oset_xor(struct zcore_oset * A, struct zcore_oset const * B);
#define zcore_oset_symdifference(...) zcore_oset_xor(__VA_ARGS__)

/**
 * Perform Symmetric Difference operation between two sets and
 * union the results into the third set.
 *
 * The operation can be described with set-notation as follows:
 *
 *     C := C ∪ (A ⊕ B)
 *
 * @param C set to store resulting union.
 * @param A set to apply.
 * @param B set to apply.
 * @return 0 on success.
 *         -ENOMEM if C cannot accomidate the full results of the xor of
 *         A and B. C contents are likely clobbered from a partial xor.
 */
int
zcore_oset_xor_into(struct zcore_oset * C, struct zcore_oset const * A,
                    struct zcore_oset const * B);
#define zcore_oset_symdifference_into(...) zcore_oset_xor_into(__VA_ARGS__)

/**
 * Creates new sorted array of elements contained in the set
 *
 * @param avl the avlarr to be read
 * @param elems pointer to the array pointer
 * @parma mi memory interface with which to allocate the new arrays.
 * @return length of the allocated arrays.
 */
static attr_inline size_t
zcore_oset_create_array(struct zcore_oset const * the, void *** elems,
                        struct zcore_memoryif const * mi)
{
	return zcore_avlarr_create_array(&the->avlarr, elems, NULL, mi);
}

#ifdef __cplusplus
}
#endif

#endif/*ZCORE_CONTAINERS_SET_H*/
