/** \file
 *
 * Priority Queue implementation
 *
 * Implements Priority Queue abstract container using underlying code
 * data container.
 */

#ifndef ZCORE_CONTAINERS_PRIOQ_H
#define ZCORE_CONTAINERS_PRIOQ_H

#include "zcore/config.h"
#include "zcore/stdinc.h"
#include "zcore/containers/avlarr.h"

#ifdef __cplusplus
extern "C" {
#endif

struct zcore_prioq {
	struct zcore_avlarr avlarr;
	struct zcore_destructor const * dtor;
};

extern struct zcore_destructor const * const zcore_prioq_destructor;

/**
 * Construct a Priority Queue.
 *
 * @param  the zcore_prioq to construct.
 * @param  dtor
 *           Fn to handle the transfer of ownership of the item.
 *           This can mean either freeing allocated memory that is no
 *           longer needed, or nominally ensuring the memory is still
 *           reachable elsewhere.
 * @paran  dtor_context extra data passed to the dtor
 * @param  cmp Comparator for item types.
 * @param  mi
 *           Memory interface used to dynamically allocate and resize
 *           the underlying implementation.
 *           If NULL is given, stdmi is used as a default.
 * @return 0 on success\n
 *         -EINVAL if any input is invalid or illformed\n
 *         -ENOMEM if map cannot be constructed due to insufficient memory.
 */
static attr_inline int
zcore_prioq_construct(struct zcore_prioq * the,
                      struct zcore_destructor const * dtor,
                      struct zcore_comparer const * cmp,
                      struct zcore_memoryif const * mi)
{
	the->dtor = dtor;
	return zcore_avlarr_construct(&the->avlarr, false,
	                              ZCORE_PRIOQ_INITIAL_HEIGHT, cmp, mi);
}

/**
 * Destruct a Priority Queue.
 * First applying passed dtor to every remaining element in the map.
 *
 * @param  the zcore_prioq to destuct
 */
static attr_inline void
zcore_prioq_destruct(struct zcore_prioq * the)
{
	zcore_avlarr_destruct(&the->avlarr, the->dtor, NULL);
}

/**
 * Clear the internal storage of the container.
 * This does not call destructors on held items, or destruct the container.
 *
 * @param the oset to flush
 */
static attr_inline void
zcore_prioq_flush(struct zcore_prioq * the)
{
	zcore_avlarr_flush(&the->avlarr);
}

/**
 * Insert an item into the prioqueue.
 *
 * If an item that compares equal is already in the set, it will be deleted
 * and replaced by the new item.
 *
 * @param  the zcore_prioq to insert into
 * @param  itme to store in the prioqueue.
 * @return 0 on success\n
 *         -EINVAL if item is NULL.\n
 *         -ENOMEM if queue cannot accomidate the given item.
 */
int
zcore_prioq_insert(struct zcore_prioq * the, void const * item);

/**
 * Checks if an item is contained within the queue.
 *
 * @param the queue being examined.
 * @param item to check belongs to the queue.
 * @return true, if item is in the queue\n
 *         false, if item is not in the queue.
 */
static attr_inline bool
zcore_prioq_contains(struct zcore_prioq const * the, void const * item)
{
	if (zcore_avlarr_lookup_idx(&the->avlarr, item) != SIZE_MAX)
		return true;
	else
		return false;
}

/**
 * Pop and Delete an item from the top of the queue.
 *
 * @param  the queue being modified.
 * @return pointer to item at the top of the queue.
 */
void
zcore_prioq_delete_high(struct zcore_prioq * the);

/**
 * Pop and Delete an item from the top of the queue.
 *
 * @param  the queue being modified.
 * @return pointer to item at the top of the queue.
 */
void
zcore_prioq_delete_low(struct zcore_prioq * the);

/**
 * Pop an item from the top of the queue.
 *
 * @param  the queue being modified.
 * @return pointer to item at the top of the queue.
 */
void *
zcore_prioq_pop_high(struct zcore_prioq * the);

/**
 * Pop an item from the top of the queue.
 *
 * @param  the queue being modified.
 * @return pointer to item at the top of the queue.
 */
void *
zcore_prioq_pop_low(struct zcore_prioq * the);

/**
 * Peak on the top (highest compare order) item in the queue.
 *
 * @param the queue being modified.
 * @return the item on the top of the queue
 */
void *
zcore_prioq_peak_high(struct zcore_prioq const * the);

/**
 * Peak on the top (highest compare order) item in the queue.
 *
 * @param the queue being modified.
 * @return the item on the top of the queue
 */
void *
zcore_prioq_peak_low(struct zcore_prioq const * the);

/**
 * Number of items within the queue
 */
static attr_inline size_t
zcore_prioq_size(struct zcore_prioq const * the)
{
	return the->avlarr.count;
}

/**
 * Optimizes zcore_prioq.
 *
 * WARNING: Operation has space complexity O(n) while running.
 *
 * @param  the zcore_prioq to optimize
 * @param  trim
 *           Trim down the capacity to the minimum size necessary
 *           to hold the newly optimized tree. This is helpful if no more
 *           insertions or deletions will be performed for some time.
 * @return
 *         Returns 0 upon success.
 *         Returns -ENOMEM upon failure.
 */
static attr_inline int
zcore_prioq_optimize(struct zcore_prioq * the, bool trim)
{
	return zcore_avlarr_optimize(&the->avlarr, trim);
}

/**
 * Creates new sorted array of elements contained in the queue

 * @param avl the avlarr to be read
 * @param elems pointer to the array pointer
 * @parma mi memory interface with which to allocate the new arrays.
 * @return length of the allocated arrays.
 */
static attr_inline size_t
zcore_prioq_create_array(struct zcore_prioq const * the, void *** elems,
                         struct zcore_memoryif const * mi)
{
	return zcore_avlarr_create_array(&the->avlarr, elems, NULL, mi);
}

#ifdef __cplusplus
}
#endif

#endif/*ZCORE_CONTAINERS_OMAP_H*/
