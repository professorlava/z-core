#ifndef ZCORE_DESTRUCTOR_H
#define ZCORE_DESTRUCTOR_H

#include "zcore/attributes.h"

#ifdef __cplusplus
extern "C" {
#endif

struct zcore_destructor {
	/**
	 * Destroy a thing.
	 *
	 * @param the destructor that contains the called function pointer.
	 * @param ptr the thing to destroy.
	 */
	void (* destruct)(
		struct zcore_destructor const * the,
		void * ptr);
};

#define ZCORE_DESTRUCTOR_INIT(dtor) \
	(struct zcore_destructor) { \
		.destruct=(dtor), \
	}

#define ZCORE_DESTRUCTOR_DECLARE(name) struct zcore_destructor name

#define ZCORE_DESTRUCTOR_DEFINE(name, dtor) \
	ZCORE_DESTRUCTOR_DECLARE(name) = ZCORE_DESTRUCTOR_INIT(dtor)

/**
 * Destory a thing.
 *
 * @param the destructor that contains the called function pointer.
 * @param ptr the thing to destory.
 */
static attr_inline void
zcore_destruct(struct zcore_destructor const * the, void * ptr)
{
	if (NULL != the && NULL != the->destruct)
		the->destruct(the, ptr);
}


#ifdef __cplusplus
}
#endif

#endif/*ZCORE_DESTRUCTOR_H*/
