/*
 * Macro/inline string hash
 * From somewhere online
 *
 * Fast hashing routine for ints,  longs and pointers:
 * Copyright (C) 2002 Nadia Yvette Chambers, IBM
 *
 * Both adapted as needed for zcore
 */
#ifndef ZCORE_HASH_H
#define ZCORE_HASH_H

#include <zcore/config.h>
#include <zcore/attributes.h>
#include <zcore/stdinc.h>

#if (ZCORE_BITS_PER_PTR != 32) && (ZCORE_BITS_PER_PTR != 64)
#  error "Ptr not 32 or 64! how did this happen?"
#endif

static attr_inline uint32_t
zcore_str_hash_32(char const * string, size_t len)
{
	uint32_t hash = 0;
	size_t i;
	for(i = 0; i < len; ++i)
		hash = 65599 * hash + string[i];
	return hash ^ (hash >> 16);
}

static attr_inline uint64_t
zcore_str_hash_64(char const * string, size_t len)
{
	uint64_t hash = 0;
	size_t i;
	for(i = 0; i < len; ++i)
		hash = 65599 * hash + string[i];
	return hash ^ (hash >> 32);
}

#if ZCORE_BITS_PER_PTR == 32
#  define zcore_str_hash(s,l) zcore_str_hash_32(s,l)
#elif ZCORE_BITS_PER_PTR == 64
#  define zcore_str_hash(s,l) zcore_str_hash_64(s,l)
#endif

#define _H1(s,i,x) (x*65599ull+(uint8_t)s[(i)<strlen(s)?strlen(s)-1-(i):strlen(s)])
#define _H4(s,i,x) _H1(s,i,_H1(s,i+1,_H1(s,i+2,_H1(s,i+3,x))))
#define _H16(s,i,x) _H4(s,i,_H4(s,i+4,_H4(s,i+8,_H4(s,i+12,x))))
#define _H64(s,i,x) _H16(s,i,_H16(s,i+16,_H16(s,i+32,_H16(s,i+48,x))))
#define _H256(s,i,x) _H64(s,i,_H64(s,i+64,_H64(s,i+128,_H64(s,i+192,x))))
#define ZCORE_STR_HASH_64(s) ((uint64_t)(_H256(s,0,0)^(_H256(s,0,0)>>32)))
#define ZCORE_STR_HASH_32(s) ((uint32_t)(_H256(s,0,0)^(_H256(s,0,0)>>16)))

#if ZCORE_BITS_PER_PTR == 32
#  define ZCORE_STR_HASH(s) ZCORE_STR_HASH_32(s)
#elif ZCORE_BITS_PER_PTR == 64
#  define ZCORE_STR_HASH(s) ZCORE_STR_HASH_64(s)
#endif

/* 2^21 + 2^29 - 2^25 + 2^22 - 2^19 - 2^16 + 1 */
#define ZCORE_GOLDEN_RATIO_PRIME_32 0x9e370001UL
/*  2^63 + 2^61 - 2^57 + 2^54 - 2^51 - 2^18 + 1 */
#define ZCORE_GOLDEN_RATIO_PRIME_64 0x9e37fffffffc0001UL

#if ZCORE_BITS_PER_PTR == 32
#  define ZCORE_GOLDEN_RATIO_PRIME ZCORE_GOLDEN_RATIO_PRIME_32
#  define zcore_hash(val, bits) zcore_hash_32(val, bits)
#elif ZCORE_BITS_PER_PTR == 64
#  define ZCORE_GOLDEN_RATIO_PRIME ZCORE_GOLDEN_RATIO_PRIME_64
#  define zcore_hash(val, bits) zcore_hash_64(val, bits)
#endif

static attr_inline uint64_t
zcore_hash_64(uint64_t val, unsigned int bits)
{
	register uint64_t hash = val;
/* supposed to be bits per long */
#if ZCORE_BITS_PER_LONG == 64
	hash = hash * ZCORE_GOLDEN_RATIO_PRIME_64;
#else
	register uint64_t n = hash;
	n <<= 18;
	hash -= n;
	n <<= 33;
	hash -= n;
	n <<= 3;
	hash += n;
	n <<= 3;
	hash -= n;
	n <<= 4;
	hash += n;
	n <<= 2;
	hash += n;
#endif
	/* High bits are more random, so use them */
	return hash >> (64 - bits);
}

static attr_inline uint32_t
zcore_hash_32(uint32_t val, unsigned int bits)
{
	/* On some cpus multiply is faster, on others gcc will do shifts */
	register uint32_t hash = val * ZCORE_GOLDEN_RATIO_PRIME_32;
	/* High bits are more random, so use them. */
	return hash >> (32 - bits);
}

#endif/*ZCORE_HASH_H*/
