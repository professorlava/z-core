#ifndef ZCORE_MEMORY_H
#define ZCORE_MEMORY_H

#include "zcore/stdinc.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Memory Management interface
 */
struct zcore_memoryif {
	/**
	 * Free memory block pointed to by passed pointer.
	 *
	 * @param this    pointer to memory_interface that contains the called
	 *          function pointer.
	 * @param ptr     pointer to block of memory to free.
	 */
	void (* free) (
		struct zcore_memoryif const * this,
		void * ptr);

	/**
	 * Allocate memory block of certain size and alignment.
	 *
	 * @param this  pointer to memory_interface that contains the called
	 *          function pointer.
	 * @param size  number units of memory to allocate in this block,
	 *          in unit_size.
	 * @param align alignment of memory block to allocate. May be ignored
	 *          if memory_interface implementation does not support
	 *          alignment.
	 */
	void * (* alloc) (
		struct zcore_memoryif const * this,
		size_t size,
		size_t alignment);

	/**
	 * Reallocate memory block of certain size and alignment.
	 *
	 * @param this    pointer to memory_interface that contains the called
	 *          function pointer.
	 * @param ptr     pointer to block of memory to reallocate.
	 * @param size    number units of memory to allocate in this block, in
	 *          unit_size.
	 * @param align   alignment of memory block to allocate. May be ignored
	 *          if memory_interface implementation does not support
	 *          alignment.
	 */
	void * (* realloc) (
		struct zcore_memoryif const * this,
		void * ptr,
		size_t size,
		size_t alignment);

	/**
	 * Get the name of the allocator implementation.
	 *
	 * @return null terminated string
	 */
	char const * (* instance_of) (void);

	size_t unit_size; /**< number of bytes in each allocation unit */
};

/**
 * Free memory pointed to by passed pointer.
 *
 * @param the   pointer to memory_interface.
 * @param ptr   pointer to block of memory to free.
 */
static attr_inline void
zcore_free(struct zcore_memoryif const * the, void * ptr)
{
	the->free(the, ptr);
}

/**
 * Allocate memory block of certain size and alignment.
 *
 * @param this  pointer to memory_interface that contains the called
 *          function pointer.
 * @param size  number of BYTES of memory to allocate.
 * @param align alignment of memory block to allocate. May be ignored
 *          if memory_interface implementation does not support
 *          alignment.
 */
static attr_inline void *
zcore_alloc(struct zcore_memoryif const * the, size_t size, size_t align)
{
	return the->alloc(the, size / the->unit_size, align);
}

/**
 * Reallocate memory of certain size and alignment.
 *
 * @param this    pointer to memory_interface.
 * @param ptr     pointer to memory to reallocate.
 * @param size    number of BYTES of memory to allocate.
 * @param align   alignment of memory block to allocate. May be ignored
 *          if memory_interface implementation does not support
 *          alignment.
 */
static attr_inline void *
zcore_realloc(struct zcore_memoryif const * the, void * ptr, size_t size,
              size_t align)
{
	return the->realloc(the, ptr, size / the->unit_size, align);
}

static attr_inline char const *
zcore_instance_of_memoryif(struct zcore_memoryif const * the)
{
	return the->instance_of();
}

/**
 * Implements memoryif using standard libc allocation functions.
 */
extern struct zcore_memoryif const * const stdmi;

#ifdef __cplusplus
}
#endif

#endif /*ZCORE_MEMEORY_H */
