
/**
 * Fast Efficient Fixed-Size Object pooling
 * https://www.thinkmind.org/download.php?articleid=computation_tools_2012_1_10_80006
 */

#ifndef ZCORE_POOL_H
#define ZCORE_POOL_H

#include "zcore/stdinc.h"
#include "zcore/memory.h"
#include "zcore/containers/ifwlist.h"

#ifdef __cplusplus
extern "C" {
#endif

#define POOL_PAGE_MEM_MAX USHRT_MAX

struct zcore_pool_page {
	struct zcore_ifwlist_node pages;
	uint16_t freed_count;
	uint16_t init_count;
	void * next; /**< pointer to the next free unit */
	uint8_t mem[0];
};

struct zcore_pool {
	/** Base memory interface used to allocate internal pool */
	struct zcore_memoryif const * base;
	uint16_t unit_size; /**< size units */
	uint16_t unit_count; /**< Number of units total in each page */
	ZCORE_IFWLIST_DECLARE(pages);
};

/**
 * Initialize a pool.
 *
 * @param the pool to initialize
 * @param unit_size size of block to allocate on each call to alloc.
 * @param mi the memory interface used to allocate underlying pool
 */
int
zcore_pool_initialize(struct zcore_pool * the, uint16_t unit_size,
                      struct zcore_memoryif const * mi);

/**
 * Free all pooled memory.
 *
 * @param the pool to release
 */
void
zcore_pool_destruct(struct zcore_pool * the);

/**
 * Allocate a chunk of memory, size determined by the pool from which
 * the memory is allocated.
 *
 * @param the pool to allocate from
 */
void *
zcore_pool_alloc(struct zcore_pool * the);

/**
 * Release a chunk of memory back into the memory pool.
 *
 * @param the pool the ptr was allocated from
 * @param the ptr to release
 */
void
zcore_pool_free(struct zcore_pool * the, void * ptr);

#ifdef __cplusplus
}
#endif

#endif/*ZCORE_POOL_H*/
