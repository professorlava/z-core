#ifndef ZCORE_STDINC_H
#define ZCORE_STDINC_H

/**
 * \file
 *
 * This is a general header that includes common portions of the
 * C standard library.
 *
 * #include <limits.h>
 * #include <float.h>
 *     min and max sizes for standard language types. This really should be a
 *     language feature.
 *
 * #include <stdarg.h>
 *     Variadic functions are language features, but handling them is platform
 *     specific so for some reason handling them is a library feature instead.
 *
 * #include <stdalign.h>
 * #include <stdbool.h>
 *     Defines bool and alignment operators that are part of the standard but
 *     need typedefs for forward compatibility.
 *
 * #include <stddef.h>
 *     stddef defines NULL, size_t, and offsetof. These are incredibly useful
 *     in just about any context. It is astonishing that NULL and size_t are
 *     not features. size_t is a type guaranteed to be able to represent any
 *     object size in bytes, and is therefor the correct type for indexing,
 *     counts, and sizes. offsetof is a very useful macro for a very ugly
 *     operation.
 *
 * #include <stdint.h>
 * #include <inttypes.h>
 *     stdint defines integral type aliases for specific widths, as well as
 *     limit macros. It is crazy that specific-witdh-types are not a language
 *     feature. inttypes adds additional support for specific-width types
 *     included in stdint.h
 *
 * #include <math.h>
 * #include <complex.h>
 *     Common and Complex computation operations and transforms. Why not.
 *
 * #include <errno.h>
 * #include <stdlib.h>
 * #include <string.h>
 *    Between each of these includes, scores of useful miscellaneous macros, types, and
 *    functions are included. The symbols for rand, and memory are explicitly removed to
 *    enforce the use of zcore library memory and randomization functions.
 *
 *
 * EXPLICITLY NOT INCLUDED:
 * <assert.h>
 * <ctype.h>
 * <fenv.h>
 * <iso646.h>
 * <locale.h>
 * <setjmp.h>
 * <signal.h>
 * <stdatomic.h>
 * <stdio.h>
 * <stdnoreturn.h>
 * <thmath.h>
 * <threads.h>
 * <time.h>
 * <uchar.h>
 * <wchar.h>
 * <wctype.h>
 */

#include <limits.h>
#include <float.h>

#include <stdarg.h>

#include <stdalign.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <inttypes.h>

#include <math.h>
#include <complex.h>

#include <errno.h>
#include <stdlib.h>
#include <string.h>

#include "zcore/attributes.h"

#if defined(__GNUC__)
#  define likely(x)   __builtin_expect(!!(x), 1)
#  define unlikely(x) __builtin_expect(!!(x), 0)
#else
#  define likely(x)
#  define unlikely(x)
#endif

#if !defined(typeof)
#  if defined(__typeof__) || defined(__GNUC__)
#    define typeof(x) __typeof__(x)
#  elif defined(decltype) || defined(__MSC_VER)
#    define typeof(x) decltype(x)
#  else
#    error Compiler does not support typeof()
#  endif
#endif

#if !defined(offsetof)
#  define offsetof(type, member) ((size_t) &((type *)0)->member)
#endif

#define zcore_offsetof(type, member) offsetof(type, member)

#define zcore_offsetofend(type, member) \
	(offsetof(type, member) + sizeof(((type *)0)->member))

/**
 * cast a memver of a struct out to the containing structure
 */
#define zcore_containerof(ptr, type, member) ({ \
	typeof(((type *)0)->member) const * _ptr = (ptr); \
	(type *)((char *)_ptr - offsetof(type, member)); })

#define zcore_containerof_safe(ptr, type, member) ({ \
	typeof(ptr) __ptr = (ptr); \
	__ptr ? zcore_containerof(__ptr, type, member) : NULL; })

/**
 * min()/max()/clamp() macros that also do strict type-checking..
 *
 * These macros do strict typechecking make sure values are of the
 * same type. See the "unnecessary" pointer comparisons.
 */
#define zcore_min(a, b) ({ \
	typeof(a) _mina = (a); \
	typeof(b) _minb = (b); \
	(void) (&_mina == &_minb); \
	_mina < _minb ? _mina : _minb; })

#define zcore_max(a, b) ({ \
	typeof(a) _maxa = (a); \
	typeof(b) _maxb = (b); \
	(void) (&_maxa == &_maxb); \
	_maxa > _maxb ? _maxa : _maxb; })

#define zcore_min3(a, b, c) zcore_min((typeof(a))zcore_min(a, b), c)
#define zcore_max3(a, b, c) zcore_min((typeof(a))zcore_max(a, b), c)

#define zcore_min_nz(a, b) ({ \
	typeof(a) _mnza = (a); \
	typeof(b) _mnzb = (b); \
	_mnza == 0 ? _mnzb : ((_mnzb == 0) ? _mnza : zcore_min(_mnza, _mnzb)); })

#define zcore_clamp(val, lo, hi) \
	zcore_min((typeof(val))zcore_max(lo, val), hi)

/**
 * tmin()/tmax()/tclamp() macros with less strict typechecking
 *
 * These macros do no typechecking and uses temporary variables of type
 * 'type' to make all the comparisons.
 */
#define zcore_tmin(type, a, b) ({ \
	type _mina = (a); \
	type _minb = (b); \
	_mina < _minb ? _mina : _minb; })

#define zcore_tmax(type, a, b) ({ \
	type _maxa = (a); \
	type _maxb = (b); \
	_maxa > _maxb ? _maxa : _maxb; })

#define zcore_tmin3(type, a, b, c) zcore_tmin(type, zcore_tmin(a, b), c)
#define zcore_tmax3(type, a, b, c) zcore_tmin(type, zcore_tmax(a, b), c)

#define zcore_tmin_nz(type, a, b) ({ \
	type _mnza = (a); \
	type _mnzb = (b); \
	_mnza == 0 ? _mnzb \
	           : ((_mnzb == 0) ? _mnza : zcore_tmin(type, _mnza, _mnzb)); })

#define zcore_tclamp(type, val, lo, hi) \
	zcore_tmin(type, zcore_tmax(type, lo, val), hi)

/**
 * return a value clamped to a given range using val's type
 *
 * This macro does no typechecking and uses temporary variables of whatever
 * type the input argument 'val' is.  This is useful when val is an unsigned
 * type and min and max are literals that will otherwise be assigned a signed
 * integer type.
 */
#define zcore_clamp_val(val, lo, hi) zcore_tclamp(typeof(val), val, lo, hi)

/** XXX Side effects? */
#define zcore_swap(a, b) ({ \
	typeof(a) _tmp = (a); \
	(a) = (b); \
	(b) = _tmp; })

#define zcore_is_powerof2(x) ((bool)(x && !(x & (x - 1))))

/*
 * Multiplies an integer by a fraction, while avoiding unnecessary
 * overflow or loss of precision.
 */
#define zcore_mult_frac(x, numer, denom) ({ \
	typeof(x) quot = (x) / (denom); \
	typeof(x) rem  = (x) % (denom); \
	(quot * (numer)) + ((rem * (numer)) / (denom)); })

#define zcore_ilog2(a) ({ \
	typeof(a) _cnt = (a); \
	typeof(a) _tmp = 0; \
	while(_cnt >>= 1) ++_tmp; \
	_tmp; })

#endif/*ZCORE_STDINC_H*/
