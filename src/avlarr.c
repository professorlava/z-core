#include <assert.h>
#include "zcore/stdinc.h"
#include "zcore/attributes.h"
#include "zcore/config.h"
#include "zcore/containers/avlarr.h"

/** Calculate the index of the left child based on binary heap structure */
static attr_const size_t
_left_child(size_t idx)
{
	return (idx * 2) + 1;
}

/** Calculate the index of the right child based on binary heap structure */
static attr_const size_t
_right_child(size_t idx)
{
	return (idx * 2) + 2;
}

/** Calculate the index of the other child of this nodes parent
 *  Precondition: idx != 0
 */
static attr_const size_t
_sibling(size_t idx)
{
	return ((idx & 1) ? 1 : -1) + idx;
}

/** Calculate the index of the parent based on binary heap structure */
static attr_const size_t
_parent(size_t idx)
{
	return (idx - 1) / 2;
}

static attr_const int
_default_cmp(void const * a, void const * b)
{
	uintptr_t w1 = (uintptr_t) a;
	uintptr_t w2 = (uintptr_t) b;

	if (w1 > w2)
		return 1;
	if (w1 < w2)
		return -1;
	return 0;
}

static attr_pure int
_user_cmp(struct zcore_avlarr const * avl, void const * a, void const * b)
{
	return zcore_compare(avl->key_cmp, a, b);
}

attr_pure size_t
zcore_avlarr_first_ordered_idx(struct zcore_avlarr const * avl)
{
	size_t prev, i;
	prev = 0;
	i = _left_child(prev);

	while (i < avl->capacity && avl->keys[i] != NULL) {
		prev = i;
		i = _left_child(prev);
	}
	return prev;
}

attr_pure size_t
zcore_avlarr_last_ordered_idx(struct zcore_avlarr const * avl)
{
	size_t prev, i;
	prev = 0;
	i = _right_child(prev);

	while (i < avl->capacity && avl->keys[i] != NULL) {
		prev = i;
		i = _right_child(prev);
	}
	return prev;
}

static attr_pure size_t
_previous_ordered_child(struct zcore_avlarr const * avl, size_t idx)
{
	size_t i, next;
	next = idx;
	i = _left_child(idx);
	/* Search right subtree */
	while (i < avl->capacity && avl->keys[i] != NULL) {
		next = i;
		i = _right_child(i);
	}
	return next;
}

attr_pure size_t
zcore_avlarr_previous_ordered_idx(struct zcore_avlarr const * avl, size_t idx)
{
	size_t i, next;

	next = _previous_ordered_child(avl, idx);
	if (next == idx) {
		/* It is not in the left subtree, go up */
		i = idx;
		next = _parent(i);
		/* the immediate parent of a left child is previous,
		   go up again until i is the right child of next, then get the
		   ordered child of that node */
		while (i != 0) {
			if (_right_child(next) == i)
				break;
			i = next;
			next = _parent(i);
		}
	}
	return next;
}

static attr_pure size_t
_next_ordered_child(struct zcore_avlarr const * avl, size_t idx)
{
	size_t i, next;
	next = idx;
	i = _right_child(idx);
	/* Search right subtree */
	while (i < avl->capacity && avl->keys[i] != NULL) {
		next = i;
		i = _left_child(i);
	}
	return next;
}

attr_pure size_t
zcore_avlarr_next_ordered_idx(struct zcore_avlarr const * avl, size_t idx)
{
	size_t i, next;

	next = _next_ordered_child(avl, idx);
	if (next == idx) {
		/* It is not in the right subtree, go up */
		i = idx;
		next = _parent(i);
		/* the immediate parent of a right child is previous,
		   go up again until i is the left child of next, then get the
		   ordered child of that node */
		while (i != 0) {
			if (_left_child(next) == i)
				break;
			i = next;
			next = _parent(i);
		}
	}
	return next;
}

/**
 * Grow the tree so that more leaves can fit!
 */
static attr_inline int
_resize_array(struct zcore_avlarr * avl, size_t size)
{
	size_t const keys_sz = size * sizeof(*avl->keys);
	size_t const values_sz = avl->values ? size * sizeof(*avl->values) : 0;
	size_t const heights_sz = size * sizeof(*avl->heights);
	size_t shift;

	if (NULL == avl->mi)
		return -ENOMEM;

	avl->keys = zcore_realloc(avl->mi, avl->keys, keys_sz, 0);
	if (values_sz)
		avl->values = zcore_realloc(avl->mi, avl->values, values_sz, 0);
	avl->heights = zcore_realloc(avl->mi, avl->heights, heights_sz, 0);
	if (unlikely(NULL == avl->keys
	          || (values_sz && (NULL == avl->values))
	          || NULL == avl->heights)) {
		if (avl->keys)
			zcore_free(avl->mi, avl->keys);
		if (avl->values)
			zcore_free(avl->mi, avl->values);
		if (avl->heights)
			zcore_free(avl->mi, avl->heights);
		avl->count = 0;
		avl->capacity = 0;
		return -ENOMEM;
	}
	if (likely(size > avl->capacity)) {
		shift = size - avl->capacity;
		memset(avl->keys + avl->capacity, 0,
		       shift * sizeof(*avl->keys));
#if defined(ZCORE_SANITARY_CONTAINERS)
		memset(avl->values + avl->capacity, 0,
		       shift * sizeof(*avl->values));
#endif
		memset(avl->heights + avl->capacity, ZCORE_AVLARR_NULL_HEIGHT,
		       shift * sizeof(*avl->heights));
	}
	avl->capacity = size;
	return 0;
}

int
zcore_avlarr_construct(struct zcore_avlarr * the, bool store_values,
                       int8_t initial_height,
                       struct zcore_comparer const * kcmp,
                       struct zcore_memoryif const * mi)
{
	size_t const size = ZCORE_AVLARR_H2C(initial_height);

	if (NULL == the || initial_height < 0)
		return -EINVAL;

	if (NULL == mi)
		mi = stdmi;

	the->keys = zcore_alloc(mi, size * sizeof(*the->keys), 0);
	if (NULL == the->keys)
		goto quit_none;
	memset(the->keys, 0, size * sizeof(*the->keys));

	if (store_values) {
		the->values = zcore_alloc(mi, size * sizeof(*the->values), 0);
		if (NULL == the->values)
			goto quit_keys;
		memset(the->values, 0, size * sizeof(*the->values));
	} else {
		the->values = NULL;
	}

	the->heights = zcore_alloc(mi, size * sizeof(*the->heights), 0);
	if (NULL == the->heights)
		goto quit_values;
	memset(the->heights, ZCORE_AVLARR_NULL_HEIGHT,
	       size * sizeof(*the->heights));

	the->count = 0;
	the->capacity = size;
	the->key_cmp = kcmp;
	the->mi = mi;
	return 0;

quit_values:
	zcore_free(mi, the->values);
quit_keys:
	zcore_free(mi, the->keys);
quit_none:
	return -ENOMEM;
}

void
zcore_avlarr_destruct(struct zcore_avlarr * the,
                      struct zcore_destructor const * key_dtor,
                      struct zcore_destructor const * val_dtor)
{
	size_t i;

	if (NULL != key_dtor || NULL != val_dtor) {
		for (i = 0; i < the->capacity; i += 1) {
			zcore_destruct(key_dtor, the->keys[i]);
			if (NULL != the->keys[i])
				zcore_destruct(val_dtor, the->values[i]);
		}
	}
	if (the->mi) {
		zcore_free(the->mi, the->keys);
		zcore_free(the->mi, the->values);
		zcore_free(the->mi, the->heights);
	}
}

void
zcore_avlarr_destructor(struct zcore_destructor const * the, void * ptr)
{
	struct zcore_avlarr_destructor * dtor;
	struct zcore_avlarr * kill;

	dtor = zcore_containerof(the, struct zcore_avlarr_destructor, base);
	kill = ptr;

	zcore_avlarr_destruct(kill, dtor->key_dtor, dtor->val_dtor);
}

void
zcore_avlarr_flush(struct zcore_avlarr * the)
{
	memset(the->keys, 0, the->capacity * sizeof(*the->keys));
	if (the->values)
		memset(the->values, 0, the->capacity * sizeof(*the->values));
	memset(the->heights, ZCORE_AVLARR_NULL_HEIGHT,
	       the->capacity * sizeof(*the->heights));
	the->count = 0;
}

/* FIXME for the love of god
 * - too long
 * - too deep */
static size_t
_lookup(struct zcore_avlarr const * avl, void const * key)
{
	size_t idx = 0;
	void * node_key;

	if (likely(NULL != key)) {
		/* check for cmp just once, we have better things if iti
		 * is not there*/
		if (avl->key_cmp) {
			int cmpres;
			while (idx < avl->capacity
			    && 0 != (node_key = avl->keys[idx])) {
				cmpres = _user_cmp(avl, key, node_key);
				if (cmpres < 0) {
					idx = _left_child(idx);
				} else if (cmpres > 0) {
					idx = _right_child(idx);
				} else {
					return idx;
				}
			}
		} else {
			/* Fast-track special case. If we dont have a special
			 * cmp function then dont bother with a function call.
			 * Just compare the keys by value */
			uintptr_t a = (uintptr_t)key;
			uintptr_t b;
			while (idx < avl->capacity
			    && 0 != (node_key = avl->keys[idx])) {
				b = (uintptr_t)node_key;
				if (a < b) {
					idx = _left_child(idx);
				} else if (a > b) {
					idx = _right_child(idx);
				} else {
					return idx;
				}
			}
		}
	}
	return SIZE_MAX;
}

attr_pure size_t
zcore_avlarr_lookup_idx(struct zcore_avlarr const * avl, void const * key)
{
	return _lookup(avl, key);
}

attr_pure void *
zcore_avlarr_lookup(struct zcore_avlarr const * avl, void const * key)
{
	size_t idx;

	if (avl->values) {
		idx = _lookup(avl, key);
		if (SIZE_MAX != idx) {
			return avl->values[idx];
		}
	}
	return NULL;
}

attr_pure int8_t
zcore_avlarr_height(struct zcore_avlarr const * avl)
{
	if (avl->capacity == 0)
		return ZCORE_AVLARR_NULL_HEIGHT;
	return avl->heights[0];
}

static attr_pure int8_t
_balance(struct zcore_avlarr const * avl, size_t idx)
{
	size_t const l_c = _left_child(idx);
	size_t const r_c = _right_child(idx);
	return avl->heights[r_c] - avl->heights[l_c];
}

attr_pure int
zcore_avlarr_balance(struct zcore_avlarr const * avl)
{
	if (avl->capacity == 0)
		return 0;
	return _balance(avl, 0);
}

static void
_fix_height(struct zcore_avlarr * avl, size_t n)
{
	size_t c1, c2;
	int8_t h1, h2;

	c1 = _left_child(n);
	c2 = _right_child(n);
	h1 = (c1 < avl->capacity) ? avl->heights[c1] : ZCORE_AVLARR_NULL_HEIGHT;
	h2 = (c2 < avl->capacity) ? avl->heights[c2] : ZCORE_AVLARR_NULL_HEIGHT;

	for (;;) {
		avl->heights[n] = 1 + ((h1 > h2) ? h1 : h2);
		if (n == 0)
			break;
		c1 = n;
		c2 = _sibling(n);
		n = _parent(n);
		/* after the initial check all heights will be valid, no need
		 * to check again */
		h1 = avl->heights[c1];
		h2 = avl->heights[c2];
	}
}

static void
_move_node(struct zcore_avlarr * avl, size_t from, size_t to)
{
	avl->keys[to] = avl->keys[from];
	avl->keys[from] = NULL;
	avl->heights[to] = avl->heights[from];
	avl->heights[from] = ZCORE_AVLARR_NULL_HEIGHT;
	if (avl->values) {
		avl->values[to] = avl->values[from];
#if defined(ZCORE_SANITARY_CONTAINERS)
		avl->values[from] = NULL;
#endif
	}
}

static void
_shift_up_left(struct zcore_avlarr * avl, size_t from, size_t to)
{
	if (unlikely(from >= avl->capacity || avl->keys[from] == 0))
		return;
	_move_node(avl, from, to);
	_shift_up_left(avl, _left_child(from),  _left_child(to));
	_shift_up_left(avl, _right_child(from), _right_child(to));
}

static void
_shift_down_left(struct zcore_avlarr * avl, size_t from, size_t to)
{
	if (unlikely(from >= avl->capacity || avl->keys[from] == 0))
		return;
	_shift_down_left(avl, _left_child(from),  _left_child(to));
	_shift_down_left(avl, _right_child(from), _right_child(to));
	if (to < avl->capacity)
		_move_node(avl, from, to);
}

/** _rotate_left
 * T1, T2 and T3 are subtrees.
 *          X                                 Y
 *         / \                               / \
 *       T1   Y      Left Rotate (X)        X  T3
 *           / \    - - - - - - - - ->     / \
 *          T2 T3                        T1  T2
 */
static void
_rotate_left(struct zcore_avlarr * avl, size_t X)
{
	size_t const l_c = _left_child(X);
	size_t const r_c = _right_child(X);
	size_t const ll_c = _left_child(l_c);
	size_t const rr_c = _right_child(r_c);
	size_t const lr_c = _right_child(l_c);
	size_t const rl_c = _left_child(r_c);

	/** Move T1 subtree out of the way to that X can take its spot */
	_shift_down_left(avl, l_c, ll_c);
	/** Move X down */
	_move_node(avl, X, l_c);
	/* After:  ___
	 *        /   \
	 *       x     y
	 *      / \   / \
	 *    T1  ?  T2 T3
	 */

	/** Move T2 subtree */
	_shift_down_left(avl, rl_c, lr_c);
	/** Move Y Up */
	_move_node(avl, r_c, X);
	/* After:   Y
	 *        /   \
	 *       X     ?
	 *      / \   / \
	 *    T1  T2 ?  T3
	 */

	/** Move T3 Subtree up */
	_shift_up_left(avl, rr_c, r_c);
	/* After:  Y
	 *        / \
	 *       X   T3
	 *      / \
	 *    T1  T2
	 */

	_fix_height(avl, l_c);
}

static void
_shift_up_right(struct zcore_avlarr * avl, size_t from, size_t to)
{
	if (unlikely(from >= avl->capacity || avl->keys[from] == 0))
		return;
	_move_node(avl, from, to);
	_shift_up_right(avl, _right_child(from), _right_child(to));
	_shift_up_right(avl, _left_child(from),  _left_child(to));
}

static void
_shift_down_right(struct zcore_avlarr * avl, size_t from, size_t to)
{
	if (unlikely(from >= avl->capacity || avl->keys[from] == 0))
		return;
	_shift_down_right(avl, _right_child(from), _right_child(to));
	_shift_down_right(avl, _left_child(from),  _left_child(to));
	if (to < avl->capacity)
		_move_node(avl, from, to);
}

/** _rotate_right
 * T1, T2 and T3 are subtrees.
 *          Y                                     X
 *         / \                                   / \
 *        X   T3      Right Rotate (y)         T1   Y
 *       / \          - - - - - - - - ->           / \
 *     T1   T2                                   T2  T3
 */
static void
_rotate_right(struct zcore_avlarr * avl, size_t Y)
{
	size_t const l_c = _left_child(Y);
	size_t const r_c = _right_child(Y);
	size_t const ll_c = _left_child(l_c);
	size_t const rr_c = _right_child(r_c);
	size_t const lr_c = _right_child(l_c);
	size_t const rl_c = _left_child(r_c);

	/** Move T3 subtree out of the way to that Y can take its spot */
	_shift_down_right(avl, r_c, rr_c);
	/** Move Y down */
	_move_node(avl, Y, r_c);
	/* After:   ?
	 *        /   \
	 *       X     Y
	 *      / \   / \
	 *    T1  T2 ?  T3
	 */

	/** Move T2 subtree */
	_shift_down_right(avl, lr_c, rl_c);
	/** Move X Up */
	_move_node(avl, l_c, Y);
	/* After:   X
	 *        /   \
	 *       ?     Y
	 *      / \   / \
	 *    T1  ?  T2  T3
	 */

	/** Move T1 Subtree up */
	_shift_up_right(avl, ll_c, l_c);
	/* After:  X
	 *        / \
	 *       T1  Y
	 *          / \
	 *        T2  T3
	 */

	_fix_height(avl, r_c);
}

static void
_rebalance(struct zcore_avlarr * avl, size_t idx)
{
	int bf, c_bf;
	size_t c;

	for (;;) {
		bf = _balance(avl, idx);
		if (bf < -1) {
			c = _left_child(idx);
			c_bf = _balance(avl, c);
			if (c_bf <= 0) {
				/* left left */
				_rotate_right(avl, idx);
			} else if( c_bf > 0) {
				/* left right */
				_rotate_left(avl, c);
				_rotate_right(avl, idx);
			}
		} else if (bf > 1) {
			c = _right_child(idx);
			c_bf = _balance(avl, c);
			if (c_bf >= 0) {
				/* right right */
				_rotate_left(avl, idx);
			} else if (c_bf < 0) {
				/* right left */
				_rotate_right(avl, c);
				_rotate_left(avl, idx);
			}
		}
		if (idx == 0)
			break;
		idx = _parent(idx);
	}
}

static attr_inline int
_add_node(struct zcore_avlarr * avl, size_t idx, void * key, void * value)
{
	if (idx >= avl->capacity) {
		if (_resize_array(avl, (avl->capacity * 2) + 1))
			return -ENOMEM;
	}
	avl->keys[idx] = key;
	if (avl->values)
		avl->values[idx] = value;
	avl->heights[idx] = 0;
	avl->count += 1;
	if (0 != idx) {
		_fix_height(avl, _parent(idx));
		_rebalance(avl, _parent(idx));
	}
	return 0;
}

static attr_pure int
_cmp_keys(struct zcore_avlarr const * avl, void const * a, void const * b)
{
	if (avl->key_cmp)
		return _user_cmp(avl, a, b);
	else
		return _default_cmp( a, b);
}

int
zcore_avlarr_insert(struct zcore_avlarr * avl, void const * key,
                    void const * value)
{
	int cmpres;
	size_t idx = 0;
	void const * nk;

	if (key == NULL)
		return -EINVAL;

	while (idx < avl->capacity) {
		nk = avl->keys[idx];

		if (nk == NULL) {
			/* found empty slot! */
			break;
		}
		cmpres = _cmp_keys(avl, key, nk);
		if (cmpres < 0) {
			idx = _left_child(idx);
		} else if (cmpres > 0) {
			idx = _right_child(idx);
		} else {
			return -EAGAIN;
		}
	}
	return _add_node(avl, idx, (void *)key, (void *)value);
}

/**
 * Purge a node from the tree.
 * This means we can assume that the node is removable, and we need to deal
 * with the logistics of its removal.
 *
 * If there are no children, there is nothing to do.
 * If there is either a left or right child, but not both, that child
 *   is shifted up.
 * If there are both children, then the heir to the removed node must
 *   be selected based on which subtree has more height (thus improving
 *   balance). After the contents of the successor are copied to the current
 *   node, the successors location is also purged using the same process.
 *
 * When the whole process is complete we want to rebalance from the deepest
 * node that was purged, since our rebalancing algorithm is bottom-up
 */
static attr_inline size_t
_purge_node(struct zcore_avlarr * avl, size_t idx)
{
	size_t l_c , r_c, heir, rebalance_at;
	void const * l_k;
	void const * r_k;

do_purge:
	l_c = _left_child(idx);
	r_c = _right_child(idx);
	l_k = (l_c < avl->capacity) ? avl->keys[l_c] : NULL;
	r_k = (r_c < avl->capacity) ? avl->keys[r_c] : NULL;
	rebalance_at = idx;
	if (NULL == l_k && NULL == r_k) {
		avl->keys[idx] = NULL;
		avl->heights[idx] = ZCORE_AVLARR_NULL_HEIGHT;
	} else if (NULL == l_k) {
		_shift_up_left(avl, r_c, idx);
	} else if (NULL == r_k) {
		_shift_up_right(avl, l_c, idx);
	} else {
		if (_balance(avl, idx) < 0) {
			heir = _previous_ordered_child(avl, idx);
		} else {
			heir = _next_ordered_child(avl, idx);
		}
		_move_node(avl, heir, idx);
		/* Since we are moving heir up we need to purge from
		 * heir's old node so that node is not empty */
		idx = heir;
		goto do_purge;
	}
	avl->count -= 1;
	return rebalance_at;
}

static void *
_remove_node(struct zcore_avlarr * avl, size_t idx, void ** key_ref)
{
	void * ret = NULL;
	size_t rebalance_at;

	if (SIZE_MAX != idx) {
		if (key_ref)
			*key_ref = avl->keys[idx];
		if (avl->values)
			ret = avl->values[idx];
		rebalance_at = _purge_node(avl, idx);
		if (rebalance_at != 0) {
			_fix_height(avl, _parent(rebalance_at));
			_rebalance(avl, _parent(rebalance_at));
		}
	} else if (key_ref) {
		*key_ref = NULL;
	}
	return ret;
}

void *
zcore_avlarr_remove_idx_key_ref(struct zcore_avlarr * avl, size_t idx,
                                void ** key_ref)
{
	void * rv = NULL;
	if (idx < avl->capacity)
		rv = _remove_node(avl, idx, key_ref);
	return rv;
}

void *
zcore_avlarr_remove_idx(struct zcore_avlarr * avl, size_t idx)
{
	return zcore_avlarr_remove_idx_key_ref(avl, idx, NULL);
}

void *
zcore_avlarr_remove_key_ref(struct zcore_avlarr * avl, void const * key,
                            void ** key_ref)
{
	size_t idx;

	idx = _lookup(avl, key);
	return _remove_node(avl, idx, key_ref);
}

void *
zcore_avlarr_remove(struct zcore_avlarr * avl, void const * key)
{
	return zcore_avlarr_remove_key_ref(avl, key, NULL);
}

/**
 * Inserting elements in compare order generates a nearly complete BST, which
 * is the desired outcome.
 */
int
zcore_avlarr_optimize(struct zcore_avlarr * avl, bool trim)
{
	struct { void const * key; void const * value;} * ntmp;
	size_t idx, cnt, tmp;

	if(avl->count > 0) {
		ntmp = zcore_alloc(stdmi, avl->count * sizeof(*ntmp), 0);
		if (NULL == ntmp)
			return -ENOMEM;
		/* Generate a sorted list by iterating tree in-order */
		cnt = 0;
		idx = zcore_avlarr_first_ordered_idx(avl);
		while (idx < avl->capacity) {
			ntmp[cnt].key = avl->keys[idx];
			if (avl->values)
				ntmp[cnt].value = avl->values[idx];
			else
				ntmp[cnt].value = NULL;
			cnt += 1;
			idx = zcore_avlarr_next_ordered_idx(avl, idx);
		}
		assert(cnt == avl->count);

		zcore_avlarr_flush(avl);
		/* Reinsert the nodes in sorted order! */
		for(idx = 0; idx < cnt; ++idx)
			zcore_avlarr_insert(avl, ntmp[idx].key,
			                    ntmp[idx].value);
		zcore_free(stdmi, ntmp);
		if (trim && NULL != avl->mi) {
			/* The minimal height of a balance bst is log2(n) */
			tmp = zcore_ilog2(cnt);
			tmp = ZCORE_AVLARR_H2C(tmp);
			if (_resize_array(avl, tmp))
				return -ENOMEM;
		}
	} else if (trim && NULL != avl->mi) {
		zcore_free(avl->mi, avl->keys);
		if (avl->values)
			zcore_free(avl->mi, avl->values);
		zcore_free(avl->mi, avl->heights);
	}
	return 0;
}

size_t
zcore_avlarr_create_array(struct zcore_avlarr const * avl,
                          void *** keys, void *** values,
                          struct zcore_memoryif const * mi)
{
	size_t idx, cnt;
	void ** k;
	void ** v;

	if (NULL == mi)
		mi = stdmi;

	if(avl->count == 0)
		return 0;

	k = zcore_alloc(mi, avl->count * sizeof(*avl->keys), 0);
	if (NULL == k)
		return 0;
	if (avl->values && values) {
		v = zcore_alloc(mi, avl->count * sizeof(*avl->values), 0);
		if (NULL == v) {
			zcore_free(mi, k);
			return 0;
		}
		*values = v;
	}
	*keys = k;
	/* Generate a sorted list by iterating tree in-order */
	cnt = 0;
	idx = zcore_avlarr_first_ordered_idx(avl);
	while (idx < avl->capacity) {
		k[cnt] = avl->keys[idx];
		if (avl->values && values)
			v[cnt] = avl->values[idx];
		cnt += 1;
		idx = zcore_avlarr_next_ordered_idx(avl, idx);
	}
	assert(cnt == avl->count);
	return cnt;
}
