#include "zcore/containers/list.h"

static attr_inline struct zcore_list_node *
_nodeof(struct zcore_ilist_head * h)
{
	return zcore_containerof(h, struct zcore_list_node, list);
}

void
zcore_list_initialize(struct zcore_list * the,
                      struct zcore_destructor const * dtor,
                      struct zcore_memoryif const * mi)
{

	the->list = ZCORE_ILIST_INIT(the->list);
	the->dtor = dtor;
	the->length = 0;
	if (mi)
		the->mi = mi;
	else
		the->mi = stdmi;
}

void
zcore_list_destruct(struct zcore_list * the)
{
	struct zcore_ilist_head * iter = NULL;
	struct zcore_ilist_head * tmp = NULL;
	struct zcore_list_node * curr;

	zcore_ilist_foreach_safe(&the->list, iter, tmp) {
		zcore_ilist_remove(iter);
		curr = _nodeof(iter);
		zcore_destruct(the->dtor, curr->item);
		zcore_free(the->mi, curr);
	}
	the->list = ZCORE_ILIST_INIT(the->list);
}

void
zcore_list_flush(struct zcore_list * the)
{
	struct zcore_ilist_head * iter = NULL;
	struct zcore_ilist_head * tmp = NULL;

	zcore_ilist_foreach_safe(&the->list, iter, tmp) {
		zcore_ilist_remove(iter);
	}
	the->list = ZCORE_ILIST_INIT(the->list);
}

static void
_zcore_list_destructor_fn(attr_unused struct zcore_destructor const * the,
                      void * ptr)
{
	struct zcore_list * kill;

	kill = ptr;

	zcore_list_destruct(kill);
}

static struct zcore_destructor const _zcore_list_destructor =
	ZCORE_DESTRUCTOR_INIT(_zcore_list_destructor_fn);

struct zcore_destructor const * const zcore_list_destructor =
	&_zcore_list_destructor;

int
zcore_list_insert(struct zcore_list * the, size_t idx, void * item)
{
	struct zcore_ilist_head * iter;
	struct zcore_list_node * nnode;

	if (idx > the->length)
		return -EINVAL;
	nnode = zcore_alloc(the->mi, sizeof(*nnode), 0);
	if (NULL == nnode)
		return -ENOMEM;
	nnode->item = item;
	nnode->list = ZCORE_ILIST_INIT(nnode->list);
	zcore_ilist_foreach(&the->list, iter) {
		if (idx <= 0)
			break;
		idx -= 1;
	}
	zcore_ilist_push(iter, &nnode->list);
	the->length += 1;
	return 0;
}

void *
zcore_list_get(struct zcore_list const * the, size_t idx)
{
	struct zcore_ilist_head * iter;
	struct zcore_list_node * node;

	if (idx >= the->length)
		return NULL;

	if (idx > the->length / 2) {
		idx = the->length - idx - 1;
		zcore_ilist_foreach_prev(&the->list, iter) {
			if (idx == 0)
				break;
			idx -= 1;
		}
	} else {
		zcore_ilist_foreach(&the->list, iter) {
			if (idx == 0)
				break;
			idx -= 1;
		}
	}
	node = _nodeof(iter);
	return node->item;
}

void *
zcore_list_remove(struct zcore_list * the, void * item)
{
	void * rv = NULL;
	struct zcore_list_node * node;

	zcore_ilist_foreach_entry(&the->list, node, list) {
		if (node->item == item) {
			zcore_ilist_remove(&(node->list));
			the->length -= 1;
			rv = node->item;
			zcore_free(the->mi, node);
			break;
		}
	}
	return rv;
}

void *
zcore_list_remove_idx(struct zcore_list * the, size_t idx)
{
	void * rv = NULL;
	struct zcore_ilist_head * iter;
	struct zcore_list_node * node;

	if (idx >= the->length)
		return NULL;

	zcore_ilist_foreach(&the->list, iter) {
		if (idx <= 0)
			break;
		idx -= 1;
	}
	zcore_ilist_remove(iter);
	the->length -= 1;
	node = _nodeof(iter);
	rv = node->item;
	zcore_free(the->mi, node);
	return rv;
}
int
zcore_list_delete(struct zcore_list * the, void * item)
{
	struct zcore_list_node * node;

	zcore_ilist_foreach_entry(&the->list, node, list) {
		if (node->item == item)
			break;
	}
	zcore_ilist_remove(&node->list);
	the->length -= 1;
	zcore_destruct(the->dtor, node->item);
	zcore_free(the->mi, node);
	return 0;
}

int
zcore_list_delete_idx(struct zcore_list * the, size_t idx)
{
	struct zcore_ilist_head * iter;
	struct zcore_list_node * node;

	if (idx >= the->length)
		return -EINVAL;

	zcore_ilist_foreach(&the->list, iter) {
		if (idx <= 0)
			break;
		idx -= 1;
	}
	zcore_ilist_remove(iter);
	the->length -= 1;
	node = _nodeof(iter);
	zcore_destruct(the->dtor, node->item);
	zcore_free(the->mi, node);
	return 0;
}

size_t
zcore_list_indexof(struct zcore_list * the, void * item)
{
	size_t idx = 0;
	struct zcore_list_node * curr;

	zcore_ilist_foreach_entry(&the->list, curr, list) {
		if (curr->item == item)
			return idx;
		idx += 1;
	}
	return SIZE_MAX;
}

size_t
zcore_list_countof(struct zcore_list * the, void * item)
{
	size_t countof = 0;
	struct zcore_list_node * curr;

	zcore_ilist_foreach_entry(&the->list, curr, list) {
		if (curr->item == item)
			countof += 1;
	}
	return countof;
}

int
zcore_list_push_front(struct zcore_list * the, void const * item)
{
	struct zcore_list_node * node;

	node = zcore_alloc(the->mi, sizeof(*node), 0);
	if (NULL == node)
		return -ENOMEM;
	node->item = (void *)item;
	node->list = ZCORE_ILIST_INIT(node->list);
	zcore_ilist_push(&the->list, &node->list);
	the->length += 1;
	return 0;
}

int
zcore_list_push_back(struct zcore_list * the, void const * item)
{
	struct zcore_list_node * node;

	node = zcore_alloc(the->mi, sizeof(*node), 0);
	if (NULL == node)
		return -ENOMEM;
	node->item = (void *)item;
	node->list = ZCORE_ILIST_INIT(node->list);
	zcore_ilist_append(&the->list, &node->list);
	the->length += 1;
	return 0;
}

void *
zcore_list_pop_front(struct zcore_list * the)
{
	void * item = NULL;
	struct zcore_list_node * node = NULL;

	if (!zcore_ilist_is_empty(&the->list)) {
		node = _nodeof(the->list.next);
		zcore_ilist_remove(the->list.next);
		the->length -= 1;
		item = node->item;
		zcore_free(the->mi, node);
	}
	return item;
}


void *
zcore_list_pop_back(struct zcore_list * the)
{
	void * item = NULL;
	struct zcore_list_node * node = NULL;

	if (!zcore_ilist_is_empty(&the->list)) {
		node = _nodeof(the->list.prev);
		zcore_ilist_remove(the->list.prev);
		the->length -= 1;
		item = node->item;
		zcore_free(the->mi, node);
	}
	return item;
}
