#include <stdlib.h>
#include <malloc.h>

#include "zcore/memory.h"

static void
_stdmi_free(struct zcore_memoryif const * attr_unused me, void * ptr)
{
	free(ptr);
}

static void *
_stdmi_alloc(struct zcore_memoryif const * attr_unused me, size_t size,
             size_t attr_unused align)
{
	return malloc(size);
}

static void *
_stdmi_realloc(struct zcore_memoryif const * attr_unused me,
               void attr_unused * ptr, size_t size, size_t attr_unused align)
{
	return realloc(ptr, size);
}

static char const *
_stdmi_instance_of(void)
{
	static char const _name[] = "stdmi";
	return _name;
}

static struct zcore_memoryif const stdmi_impl = {
	.free        = _stdmi_free,
	.alloc       = _stdmi_alloc,
	.realloc     = _stdmi_realloc,
	.unit_size   = sizeof(char),
	.instance_of = _stdmi_instance_of,
};

struct zcore_memoryif const * const stdmi = &stdmi_impl;
