#include <assert.h>

#include "zcore/containers/omap.h"

static void
_zcore_omap_destructor_fn(attr_unused struct zcore_destructor const * the,
                          void * ptr)
{
	struct zcore_omap * kill;

	kill = ptr;

	zcore_omap_destruct(kill);
}

static struct zcore_destructor const _zcore_omap_destructor =
	ZCORE_DESTRUCTOR_INIT(_zcore_omap_destructor_fn);

struct zcore_destructor const * const zcore_omap_destructor =
	&_zcore_omap_destructor;

int
zcore_omap_insert(struct zcore_omap * the, void const * key, void const * value)
{
	void * rmkey, * rmvalue;
	int rv = zcore_avlarr_insert(&the->avlarr, key, value);
	if (-EAGAIN == rv && (the->key_dtor || the->val_dtor)) {
		rmvalue = zcore_avlarr_remove_key_ref(&the->avlarr, key,
		                                      &rmkey);
		if (NULL != rmkey) {
			zcore_destruct(the->key_dtor, rmkey);
			zcore_destruct(the->val_dtor, rmvalue);
			rv = zcore_avlarr_insert(&the->avlarr, key, value);
			assert(rv != -EAGAIN);
		}
	}
	return rv;
}

void
zcore_omap_delete(struct zcore_omap * the, void const * key)
{
	void * rmkey, * rmvalue;
	rmvalue = zcore_avlarr_remove_key_ref(&the->avlarr, key, &rmkey);
	if (NULL != rmkey) {
		zcore_destruct(the->key_dtor, rmkey);
		zcore_destruct(the->val_dtor, rmvalue);
	}
}
