#include <assert.h>

#include "zcore/containers/oset.h"

static void
_zcore_oset_destructor_fn(attr_unused struct zcore_destructor const * the,
                       void * ptr)
{
	struct zcore_oset * kill;

	kill = ptr;

	zcore_oset_destruct(kill);
}

static struct zcore_destructor const _zcore_oset_destructor =
	ZCORE_DESTRUCTOR_INIT(_zcore_oset_destructor_fn);

struct zcore_destructor const * const zcore_oset_destructor =
	&_zcore_oset_destructor;

void
zcore_oset_delete(struct zcore_oset * the, void const * elem)
{
	void * rmelem;
	zcore_avlarr_remove_key_ref(&the->avlarr, elem, &rmelem);
	if (NULL != rmelem)
		zcore_destruct(the->dtor, rmelem);
}

int
zcore_oset_insert(struct zcore_oset * the, void const * elem)
{
	void * rmelem;
	int rv = zcore_avlarr_insert(&the->avlarr, elem, NULL);
	if (-EAGAIN == rv && the->dtor) {
		zcore_avlarr_remove_key_ref(&the->avlarr, elem, &rmelem);
		if (NULL != rmelem) {
			zcore_destruct(the->dtor, rmelem);
			rv = zcore_avlarr_insert(&the->avlarr, elem, NULL);
			assert(rv != -EAGAIN);
		}
	}
	return rv;
}

bool
zcore_oset_is_subset(struct zcore_oset const * A, struct zcore_oset const * B)
{
	size_t i;
	void * elem;

	/* Iterate recklessly through elements, order does not matter*/
	for (i = 0; i < A->avlarr.capacity; ++i) {
		elem = A->avlarr.keys[i];
		if (NULL != elem && !zcore_oset_contains(B, elem))
			return false;
	}
	return true;
}

bool
zcore_oset_is_proper_subset(struct zcore_oset const * A,
                            struct zcore_oset const * B)
{
	return zcore_oset_is_subset(A, B) && A->avlarr.count != B->avlarr.count;
}

bool
zcore_oset_is_disjoint(struct zcore_oset const * A, struct zcore_oset const * B)
{
	return !zcore_oset_is_subset(A, B) && !zcore_oset_is_subset(B, A);
}

bool
zcore_oset_is_intersecting(struct zcore_oset const * A,
                           struct zcore_oset const * B)
{
	size_t i;
	void * elem;

	/** Linear iter through the smaller, binary search the larger */
	if (A->avlarr.count > B->avlarr.count)
		zcore_swap(A, B);

	for (i = 0; i < A->avlarr.capacity; ++i) {
		elem = A->avlarr.keys[i];
		if (NULL != elem && zcore_oset_contains(B, elem))
			return true;
	}
	return false;

}

int
zcore_oset_or(struct zcore_oset * A, struct zcore_oset const * B)
{
	size_t i;
	void * elem;

	/* Iterate recklessly through elements, order does not matter*/
	for (i = 0; i < B->avlarr.capacity; ++i) {
		elem = B->avlarr.keys[i];
		if (-ENOMEM == zcore_oset_insert(A, elem))
			return -ENOMEM;
	}
	return 0;
}

int
zcore_oset_or_into(struct zcore_oset * C, struct zcore_oset const * A,
                   struct zcore_oset const * B)
{
	if (-ENOMEM == zcore_oset_or(C, A))
		return -ENOMEM;
	return zcore_oset_or(C, B);
}


int
zcore_oset_and(struct zcore_oset * A, struct zcore_oset const * B)
{
	size_t i;
	void * elem;

	/* Iterate recklessly through elements, order does not matter*/
	for (i = 0; i < A->avlarr.capacity; ++i) {
		elem = A->avlarr.keys[i];
		if (NULL != elem && !zcore_oset_contains(B, elem)) {
			elem = zcore_oset_remove(A, elem);
			if (elem)
				zcore_destruct(A->dtor, elem);
		}
	}
	return 0;
}

int
zcore_oset_and_into(struct zcore_oset * C, struct zcore_oset const * A,
                    struct zcore_oset const * B)
{
	size_t i;
	void * elem;

	/** Linear iter through the smaller, binary search the larger */
	if (A->avlarr.count > B->avlarr.count)
		zcore_swap(A, B);

	for (i = 0; i < A->avlarr.capacity; ++i) {
		elem = A->avlarr.keys[i];
		if (zcore_oset_contains(B, elem)) {
			if (-ENOMEM == zcore_oset_insert(C, elem))
				return -ENOMEM;
		}
	}
	return 0;
}

int
zcore_oset_sub(struct zcore_oset * A, struct zcore_oset const * B)
{
	size_t i;
	void * elem;

	/* Iterate recklessly through elements, order does not matter*/
	for (i = 0; i < B->avlarr.capacity; ++i) {
		elem = B->avlarr.keys[i];
		if (zcore_oset_contains(A, elem)) {
			elem = zcore_oset_remove(A, elem);
			if (elem)
				zcore_destruct(A->dtor, elem);
		}
	}
	return 0;
}

int
zcore_oset_sub_into(struct zcore_oset * C, struct zcore_oset const * A,
                    struct zcore_oset const * B)
{
	size_t i;
	void * elem;

	for (i = 0; i < A->avlarr.capacity; ++i) {
		elem = A->avlarr.keys[i];
		if (elem != NULL && !zcore_oset_contains(B, elem)) {
			if (-ENOMEM == zcore_oset_insert(C, elem))
				return -ENOMEM;
		}
	}
	return 0;
}

int
zcore_oset_xor(struct zcore_oset * A, struct zcore_oset const * B)
{
	size_t i;
	void * elem;

	/* Iterate recklessly through elements, order does not matter*/
	for (i = 0; i < B->avlarr.capacity; ++i) {
		elem = B->avlarr.keys[i];
		if (NULL == elem)
			continue;
		if (zcore_oset_contains(A, elem)) {
			elem = zcore_oset_remove(A, elem);
			if (elem)
				zcore_destruct(A->dtor, elem);
		} else if (-ENOMEM == zcore_oset_insert(A, elem)) {
			return -ENOMEM;
		}
	}
	return 0;
}

int
zcore_oset_xor_into(struct zcore_oset * C, struct zcore_oset const * A,
                    struct zcore_oset const * B)
{
	int j;
	size_t i;
	void * elem;

	for (j = 0; j < 2; ++j, zcore_swap(A, B)) {
		/* Iterate recklessly through elements, order does not matter*/
		for (i = 0; i < A->avlarr.capacity; ++i) {
			elem = A->avlarr.keys[i];
			if (NULL != elem && !zcore_oset_contains(B, elem))
				if (-ENOMEM == zcore_oset_insert(C, elem))
					return -ENOMEM;
		}
	}
	return 0;

}
