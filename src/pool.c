
#include "zcore/pool.h"

int
zcore_pool_initialize(struct zcore_pool * the, uint16_t unit_size,
                      struct zcore_memoryif const * mi)
{
	if (NULL == the || unit_size < sizeof(uint16_t))
		return -EINVAL;
	if (NULL == mi)
		mi = stdmi;
	the->unit_count = POOL_PAGE_MEM_MAX / unit_size;
	if (!the->unit_count)
		return -EINVAL;
	the->base = mi;
	the->unit_size = unit_size;
	zcore_ifwlist_initialize(&the->pages);
	return 0;
}

void
zcore_pool_destruct(struct zcore_pool * the)
{
	struct zcore_pool_page * page;
	struct zcore_pool_page * tmp;

	zcore_ifwlist_foreach_entry_safe(&the->pages, page, tmp, pages) {
		zcore_ifwlist_remove(&page->pages);
		zcore_free(the->base, page);
	}
}

static struct zcore_pool_page *
_new_page(struct zcore_pool * the)
{
	size_t page_size;
	struct zcore_pool_page * page;

	page_size = the->unit_size * the->unit_count;
	page = zcore_alloc(the->base, page_size + sizeof(*page), 0);
	if (NULL == page)
		return NULL;
	memset(page->mem, 0, page_size);
	zcore_ifwlist_node_initialize(&page->pages);
	page->freed_count = the->unit_count;
	page->init_count = 0;
	page->next = page->mem;
	zcore_ifwlist_push(&the->pages, &page->pages);
	return page;
}

static struct zcore_pool_page *
_partial_page(struct zcore_pool * the)
{
	struct zcore_pool_page * page;

	zcore_ifwlist_foreach_entry(&the->pages, page, pages) {
		if (page->freed_count > 0)
			return page;
	}
	return NULL;
}

static void *
_alloc_from_page(struct zcore_pool const * the, struct zcore_pool_page * page)
{
	void * tmp;
	uint16_t n;

	if (page->init_count < the->unit_count) {
		tmp = page->mem + (page->init_count * the->unit_size);
		*((uint16_t *)tmp) = ++page->init_count;
	}
	tmp = NULL;
	if (page->freed_count > 0) {
		tmp = page->next;
		if (--page->freed_count == 0) {
			page->next = NULL;
		} else {
			n = *(uint16_t *)page->next;
			page->next = page->mem + (n * the->unit_size);
		}
	}
	return tmp;
}

static bool
_is_unit_in_page(struct zcore_pool const * the, struct zcore_pool_page * page,
                 void * ptr)
{
	void * mem_end;

	mem_end = page->mem + (the->unit_size * the->unit_count);
	return ((void *)page->mem <= ptr) && (ptr < mem_end);
}

/**
 * Precondition: ptr is in pages pool
 */
static void
_free_from_page(struct zcore_pool const * the, struct zcore_pool_page * page,
                void * ptr)
{
	uint16_t n;
	if (page->next != NULL)
		n = (page->next - (void *)page->mem) / the->unit_size;
	else
		n = the->unit_count;
	*(uint16_t *)ptr = n;
	page->next = ptr;
	page->freed_count += 1;
}

void *
zcore_pool_alloc(struct zcore_pool * the)
{
	struct zcore_pool_page * page = NULL;

	if (NULL == (page = _partial_page(the)))
		if (NULL == (page = _new_page(the)))
			return NULL;
	return _alloc_from_page(the, page);
}

void
zcore_pool_free(struct zcore_pool * the, void * ptr)
{
	struct zcore_pool_page * page;

	zcore_ifwlist_foreach_entry(&the->pages, page, pages) {
		if (_is_unit_in_page(the, page, ptr))
			_free_from_page(the, page, ptr);
	}
}
