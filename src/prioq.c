#include <assert.h>

#include "zcore/containers/prioq.h"

static void
_zcore_prioq_destructor_fn(attr_unused struct zcore_destructor const * the,
                        void * ptr)
{
	struct zcore_prioq * kill;

	kill = ptr;

	zcore_prioq_destruct(kill);
}

static struct zcore_destructor const _zcore_prioq_destructor =
	ZCORE_DESTRUCTOR_INIT(_zcore_prioq_destructor_fn);

struct zcore_destructor const * const zcore_prioq_destructor =
	&_zcore_prioq_destructor;

int
zcore_prioq_insert(struct zcore_prioq * the, void const * item)
{
	void * rmitem;
	int rv = zcore_avlarr_insert(&the->avlarr, item, NULL);
	if (-EAGAIN == rv && the->dtor) {
		zcore_avlarr_remove_key_ref(&the->avlarr, item, &rmitem);
		if (NULL != rmitem) {
			zcore_destruct(the->dtor, rmitem);
			rv = zcore_avlarr_insert(&the->avlarr, item, NULL);
			assert(rv != -EAGAIN);
		}
	}
	return rv;
}

void
zcore_prioq_delete_high(struct zcore_prioq * the)
{
	size_t idx;
	void * item_ref = NULL;
	idx = zcore_avlarr_last_ordered_idx(&the->avlarr);
	zcore_avlarr_remove_idx_key_ref(&the->avlarr, idx, &item_ref);
	if (NULL != item_ref)
		zcore_destruct(the->dtor, item_ref);
}

void
zcore_prioq_delete_low(struct zcore_prioq * the)
{
	size_t idx;
	void * item_ref = NULL;
	idx = zcore_avlarr_first_ordered_idx(&the->avlarr);
	zcore_avlarr_remove_idx_key_ref(&the->avlarr, idx, &item_ref);
	if (NULL != item_ref)
		zcore_destruct(the->dtor, item_ref);
}

void *
zcore_prioq_pop_high(struct zcore_prioq * the)
{
	size_t idx;
	void * item_ref = NULL;
	idx = zcore_avlarr_last_ordered_idx(&the->avlarr);
	zcore_avlarr_remove_idx_key_ref(&the->avlarr, idx, &item_ref);
	return item_ref;
}

void *
zcore_prioq_pop_low(struct zcore_prioq * the)
{
	size_t idx;
	void * item_ref = NULL;
	idx = zcore_avlarr_first_ordered_idx(&the->avlarr);
	zcore_avlarr_remove_idx_key_ref(&the->avlarr, idx, &item_ref);
	return item_ref;
}

void *
zcore_prioq_peak_high(struct zcore_prioq const * the)
{
	void * rv = NULL;
	size_t idx;
	idx = zcore_avlarr_last_ordered_idx(&the->avlarr);
	if (idx < the->avlarr.capacity)
		rv = the->avlarr.keys[idx];
	return rv;
}

void *
zcore_prioq_peak_low(struct zcore_prioq const * the)
{
	void * rv = NULL;
	size_t idx;
	idx = zcore_avlarr_first_ordered_idx(&the->avlarr);
	if (idx < the->avlarr.capacity)
		rv = the->avlarr.keys[idx];
	return rv;
}
