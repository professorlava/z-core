#include <stdint.h>
#include <stdlib.h>
#include <limits.h>
#include <stdio.h>
#include <check.h>
#include <string.h>

#include "check_zcore.h"

#include "zcore/containers/avlarr.h"

int is_full(struct zcore_avlarr const * avl)
{
	if (avl->count != ZCORE_AVLARR_H2C(zcore_avlarr_height(avl))) {
		printf("Not enough elements (%lu) for its height (%d), to be full\n",
		       avl->count, zcore_avlarr_height(avl));
		printf("cap %zu\n", avl->capacity);
		printf("exp %llu\n",
		       ZCORE_AVLARR_H2C(zcore_avlarr_height(avl)));
		return 0;
	}

	size_t n_exp = (avl->count + 1) / 2;
	size_t n_leaf = (avl->capacity + 1) / 2;
	size_t i = avl->capacity-1;
	size_t stop = avl->capacity - n_leaf;
	size_t tally;

	while(n_leaf >= n_exp) {
		tally = 0;
		for (; i >= stop; --i) {
			if (avl->keys[i] != NULL)
				tally += 1;
		}
		if (tally == n_leaf) {
			/* Every leaf at this height is filled, it is full */
			return 1;
		}
		n_leaf = n_leaf / 2;
		stop -= n_leaf;
	}
	/* the tree isn't full, and/or it is missing elements */
	printf("Tree is not full");
	return 0;
}

/** greedy BST property check: does not work in all cases */
int is_ordered(struct zcore_avlarr const * avl, size_t idx)
{
	size_t lc = (idx * 2) + 1;
	size_t rc = (idx * 2) + 2;
	if (!idx)
		return 1;
	/* assuming word keys */
	int cmpr = (avl->keys[lc] < avl->keys[idx])
                   && (avl->keys[idx] < avl->keys[rc]);
	if (!cmpr)
		return 0;
	return is_ordered(avl, lc) && is_ordered(avl, rc);
}

int is_balanced(struct zcore_avlarr const * avl)
{
	return (1 >= abs(zcore_avlarr_balance(avl)));
}

/****************************************************************************
 * ALLOC/FREE TEST CASE
 ****************************************************************************/

/** Test the simple standard case for construction */
START_TEST(allocfree_simple)
	int8_t h = 3;
	struct zcore_avlarr * new = calloc(1, sizeof(struct zcore_avlarr));
	ck_assert(0 == zcore_avlarr_construct(new, true, h, NULL, NULL));
	ck_assert_ptr_ne(new, NULL);
	ck_assert_ptr_eq((void *)new->key_cmp, NULL);
	ck_assert(new->capacity == ZCORE_AVLARR_H2C(h));
	ck_assert(new->count == 0);
	ck_assert_ptr_ne(new->keys, NULL);
	ck_assert_ptr_ne(new->values, NULL);
	ck_assert_ptr_ne(new->heights, NULL);
	ck_assert_ptr_eq((void *)new->mi, (void *)stdmi);
	zcore_avlarr_destruct(new, NULL, NULL);
	free(new);
END_TEST

struct _d {
	struct zcore_destructor base;
	uint32_t cnt;
};

static void __allocfree_dtor(struct zcore_destructor const * dtor,
                             attr_unused void * data)
{
	struct _d * d = zcore_containerof(dtor, struct _d, base);
	d->cnt += 1;
}

START_TEST(allocfree_dtor)
	uint32_t expected_cnt;
	int8_t h = 5;
	struct zcore_avlarr * new = calloc(1, sizeof(struct zcore_avlarr));
	ck_assert(0 == zcore_avlarr_construct(new, true, h, NULL, NULL));
	ck_assert_ptr_ne(new, NULL);
	ck_assert(new->capacity == ZCORE_AVLARR_H2C(h));
	ck_assert(new->count == 0);
	ck_assert_ptr_ne(new->keys, NULL);
	ck_assert_ptr_ne(new->values, NULL);
	ck_assert_ptr_ne(new->heights, NULL);

	for (size_t i = 0; i < new->capacity; ++i) {
		new->keys[i] = (void*)1;
	}
	expected_cnt = new->capacity;
	struct _d d = {
		ZCORE_DESTRUCTOR_INIT(__allocfree_dtor),
		0
	};
	printf("%p\n", __allocfree_dtor);
	printf("%p\n", d.base.destruct);
	zcore_avlarr_destruct(new, &d.base, NULL);
	ck_assert_int_eq(expected_cnt,  d.cnt);
	free(new);
END_TEST

/**
 * Test case where capacity is impossible to allocate.
 * A UINT_MAX height calculates a capacity of approximately ~2^42949672959
 * so there is not way that UINT_MAX is going to be allocatable ever.
 * (Seriously there aren't enough electrons in the universe)
 */
START_TEST(allocfree_tobig)
	int8_t h = INT8_MAX;
	struct zcore_avlarr * new = calloc(1, sizeof(struct zcore_avlarr));
	ck_assert(zcore_avlarr_construct(new, true, h, NULL, NULL));
	free(new);
END_TEST

static TCase * tcase_allocfree(void)
{
	TCase * tc;
	tc = tcase_create("alloc/free");
	tcase_add_test(tc, allocfree_simple);
	tcase_add_test(tc, allocfree_tobig);
	tcase_add_test(tc, allocfree_dtor);
	return tc;
}

/****************************************************************************
 * SET-LIKE INSERTION TEST CASE
 ****************************************************************************/

START_TEST(setlike_insert_badkey)
	size_t i;
	struct zcore_avlarr * new = calloc(1, sizeof(struct zcore_avlarr));
	ck_assert(0 == zcore_avlarr_construct(new, false, 5, NULL, NULL));
	ck_assert_ptr_ne(new, NULL);
	for (i = 1; i <= ZCORE_AVLARR_H2C(6); ++i)
		zcore_avlarr_insert(new, NULL, (void*)i);
	ck_assert(new->count == 0);
	for (i = 0; i < new->capacity; ++i)
		ck_assert_ptr_eq(new->keys[i], NULL);
	zcore_avlarr_destruct(new, NULL, NULL);
	free(new);
END_TEST

START_TEST(setlike_insert_withincap)
	size_t i;
	struct zcore_avlarr * new = calloc(1, sizeof(struct zcore_avlarr));
	ck_assert(0 == zcore_avlarr_construct(new, false, 5, NULL, NULL));
	ck_assert_ptr_ne(new, NULL);
	for (i = 1; i <= ZCORE_AVLARR_H2C(4); ++i)
		zcore_avlarr_insert(new, (void*)i, NULL);
	ck_assert(new->count == ZCORE_AVLARR_H2C(4));
	ck_assert_int_eq(new->capacity, ZCORE_AVLARR_H2C(5));
	for (i = 0; i < new->count; ++i) {
		ck_assert_ptr_ne(new->keys[i], NULL);
		ck_assert(new->keys[i] <= (void*)ZCORE_AVLARR_H2C(4));
	}
	zcore_avlarr_destruct(new, NULL, NULL);
	free(new);
END_TEST

START_TEST(setlike_insert_resize)
	size_t i;
	struct zcore_avlarr * new = calloc(1, sizeof(struct zcore_avlarr));
	ck_assert(0 == zcore_avlarr_construct(new, false, 3, NULL, NULL));
	ck_assert_ptr_ne(new, NULL);
	for (i = 1; i <= ZCORE_AVLARR_H2C(4); ++i)
		zcore_avlarr_insert(new, (void*)i, (void*)1);
	ck_assert(new->capacity >= ZCORE_AVLARR_H2C(4));
	ck_assert_int_eq(new->count, ZCORE_AVLARR_H2C(4));
	for (i = 0; i < new->count; ++i) {
		ck_assert_ptr_ne(new->keys[i], NULL);
		ck_assert(new->keys[i] <= (void*)ZCORE_AVLARR_H2C(4));
	}
	zcore_avlarr_destruct(new, NULL, NULL);
	free(new);
END_TEST

static TCase * tcase_setlike_insert(void)
{
	TCase * tc;
	tc = tcase_create("set-like insertion");
	tcase_add_test(tc, setlike_insert_badkey);
	tcase_add_test(tc, setlike_insert_withincap);
	tcase_add_test(tc, setlike_insert_resize);
	return tc;
}

/****************************************************************************
 * SET-LIKE LOOKUP TEST CASE
 ****************************************************************************/

START_TEST(setlike_lookup)
	intptr_t i;
	intptr_t ret;
	struct zcore_avlarr * new = calloc(1, sizeof(struct zcore_avlarr));
	ck_assert(0 == zcore_avlarr_construct(new, false, 5, NULL, NULL));
	ck_assert_ptr_ne(new, NULL);
	for (i = 1; i <= (intptr_t)ZCORE_AVLARR_H2C(4); ++i)
		zcore_avlarr_insert(new, (void*)i, (void*)1);
	ck_assert(new->count == ZCORE_AVLARR_H2C(4));
	ck_assert_int_eq(new->capacity,ZCORE_AVLARR_H2C(5));

	ret = (intptr_t)zcore_avlarr_lookup_idx(new, (void*)(intptr_t)_i);
	ck_assert_int_ne(ret, SIZE_MAX);

	zcore_avlarr_destruct(new, NULL, NULL);
	free(new);
END_TEST

static TCase * tcase_setlike_lookup(void)
{
	TCase * tc;
	tc = tcase_create("set-like lookup");
	tcase_add_loop_test(tc, setlike_lookup, 1, ZCORE_AVLARR_H2C(4)+1);
	return tc;
}

/****************************************************************************
 * SET-LIKE REMOVE TEST CASE
 ****************************************************************************/

START_TEST(setlike_remove_badkeys)
	struct map_entry * ret;
	struct zcore_avlarr * new = calloc(1, sizeof(struct zcore_avlarr));
	ck_assert(0 == zcore_avlarr_construct(new, false, 4, NULL, NULL));

	ck_assert_ptr_ne(new, NULL);
	ck_assert_int_eq(new->count, 0);

	srand(_i);
	zcore_avlarr_remove_key_ref(new, (void *)(intptr_t)rand(), (void**)&ret);
	ck_assert_ptr_eq(ret, NULL);
	ck_assert_int_eq(new->count, 0);
	ck_assert(is_ordered(new, 0));
	ck_assert(is_balanced(new));

	zcore_avlarr_destruct(new, NULL, NULL);
	free(new);
END_TEST

START_TEST(setlike_remove_one)

	intptr_t i;
	void * ret;
	struct zcore_avlarr * new = calloc(1, sizeof(struct zcore_avlarr));
	ck_assert(0 == zcore_avlarr_construct(new, false, 5, NULL, NULL));

	ck_assert_ptr_ne(new, NULL);
	for (i = 1; i <= (intptr_t)ZCORE_AVLARR_H2C(4); ++i)
		zcore_avlarr_insert(new, (void*)i, (void*)1);
	ck_assert(new->count == ZCORE_AVLARR_H2C(4));
	ck_assert_int_eq(new->capacity,ZCORE_AVLARR_H2C(5));

	zcore_avlarr_remove_key_ref(new, (void*)(intptr_t)_i+1, (void**)&ret);

	ck_assert_ptr_ne(ret, NULL);
	ck_assert_int_eq(new->count, ZCORE_AVLARR_H2C(4)-1);
	ck_assert(is_ordered(new, 0));
	ck_assert(is_balanced(new));

	zcore_avlarr_destruct(new, NULL, NULL);
	free(new);
END_TEST

static TCase * tcase_setlike_remove(void)
{
	TCase * tc;
	tc = tcase_create("set-like remove");
	tcase_add_loop_test(tc, setlike_remove_badkeys, 0, 100);
	tcase_add_loop_test(tc, setlike_remove_one, 0, ZCORE_AVLARR_H2C(4));
	return tc;
}
/****************************************************************************
 * BALANCE PROPERTY TEST CASE
 ****************************************************************************/

START_TEST(b_inorder)
	size_t i;
	struct zcore_avlarr * aa = calloc(1, sizeof(struct zcore_avlarr));
	ck_assert(0 == zcore_avlarr_construct(aa, true, 5, NULL, NULL));

	ck_assert_ptr_ne(aa, NULL);
	for (i = 1; i <= ZCORE_AVLARR_H2C(5); ++i)
		zcore_avlarr_insert(aa, (void*)i, (void*)1);
	ck_assert(is_ordered(aa, 0));
	ck_assert(is_balanced(aa));
	ck_assert(is_full(aa));
	zcore_avlarr_destruct(aa, NULL, NULL);
	free(aa);
END_TEST

START_TEST(b_reverse)
	size_t i;
	struct zcore_avlarr * aa = calloc(1, sizeof(struct zcore_avlarr));
	ck_assert(0 == zcore_avlarr_construct(aa, true, 5, NULL, NULL));

	ck_assert_ptr_ne(aa, NULL);
	for (i = ZCORE_AVLARR_H2C(5); i > 0; --i)
		zcore_avlarr_insert(aa, (void*)i, (void*)1);
	ck_assert_int_eq(aa->count, ZCORE_AVLARR_H2C(5));
	ck_assert(is_ordered(aa, 0));
	ck_assert(is_balanced(aa));
	ck_assert(is_full(aa));
	zcore_avlarr_destruct(aa, NULL, NULL);
	free(aa);
END_TEST

START_TEST(b_random)
	size_t i, j;
	struct zcore_avlarr * aa = calloc(1, sizeof(struct zcore_avlarr));
	ck_assert(0 == zcore_avlarr_construct(aa, true, 5, NULL, NULL));

	ck_assert_ptr_ne(aa, NULL);
	srand(_i);
	for (i = 0; i < ZCORE_AVLARR_H2C(5); ++i) {
		do {
			j = rand() % 1000;
		} while(zcore_avlarr_insert(aa, (void*)j, (void*)1));
	}
	ck_assert_int_eq(aa->count, ZCORE_AVLARR_H2C(5));
	ck_assert(is_ordered(aa, 0));
	ck_assert(is_balanced(aa));
	zcore_avlarr_destruct(aa, NULL, NULL);
	free(aa);
END_TEST

static TCase * tcase_balance(void)
{
	TCase * tc;
	tc = tcase_create("balance");
	tcase_add_test(tc, b_inorder);
	tcase_add_test(tc, b_reverse);
	tcase_add_loop_test(tc, b_random, 0, 10);
	return tc;
}

/****************************************************************************
 * MAP-LIKE INSERTION TEST CASE
 ****************************************************************************/

static int mapentry_cmp(attr_unused struct zcore_comparer const * cmpr,
                        void const * a, void const * b)
{
	char const * key1 = (char const *)a;
	char const * key2 = (char const *)b;
	return strcmp(key1, key2);
}

static struct zcore_comparer mapentry_comparer = {mapentry_cmp};

struct mapentry {
	char * from;
	char * to;
};

struct mapentry items[15] = {
	{"key1","value1"},
	{"key2","value2"},
	{"key3","value3"},
	{"key4","value4"},
	{"key5","value5"},
	{"key6","value6"},
	{"key7","value7"},
	{"key8","value8"},
	{"key9","value9"},
	{"key10","value10"},
	{"key11","value11"},
	{"key12","value12"},
	{"key13","value13"},
	{"key14","value14"},
	{"key15","value15"},
};
struct mapentry bad = { NULL, "bad-entry"};

START_TEST(maplike_insert_badkey)
	size_t i;
	struct zcore_avlarr * new = calloc(1, sizeof(struct zcore_avlarr));
	ck_assert(0 == zcore_avlarr_construct(new, true, 4,
	                                      &mapentry_comparer, NULL));
	ck_assert_ptr_ne(new, NULL);
	for (i = 1; i <= ZCORE_AVLARR_H2C(6); ++i)
		zcore_avlarr_insert(new, bad.from, &bad);
	ck_assert(new->count == 0);
	for (i = 0; i < new->capacity; ++i) {
		ck_assert_ptr_eq(new->keys[i], NULL);
		ck_assert_ptr_eq(new->values[i], NULL);
	}
	zcore_avlarr_destruct(new, NULL, NULL);
	free(new);
END_TEST

START_TEST(maplike_insert_withincap)
	size_t i;
	struct zcore_avlarr * new = calloc(1, sizeof(struct zcore_avlarr));
	ck_assert(0 == zcore_avlarr_construct(new, true, 4,
	                                      &mapentry_comparer, NULL));
	ck_assert_ptr_ne(new, NULL);
	for (i = 0; i < 15; ++i)
		zcore_avlarr_insert(new, items[i].from, &items[i]);
	//zcore_avlarr_optimize(new, true);
	ck_assert(new->count == 15);
	//ck_assert_int_eq(new->capacity, ZCORE_AVLARR_H2C(4));
	for (i = 0; i < new->capacity; ++i) {
		if (new->keys[i] != NULL)
			ck_assert_ptr_ne(new->values[i], NULL);
#if 0
		else
			ck_assert_ptr_eq(&new->values[i], NULL);
#endif
	}
	zcore_avlarr_destruct(new, NULL, NULL);
	free(new);
END_TEST

START_TEST(maplike_insert_resize)
	size_t i;
	struct zcore_avlarr * new = calloc(1, sizeof(struct zcore_avlarr));
	ck_assert(0 == zcore_avlarr_construct(new, true, 0,
	                                      &mapentry_comparer, NULL));
	ck_assert_ptr_ne(new, NULL);
	ck_assert_ptr_ne(new->keys, NULL);
	ck_assert_ptr_ne(new->values, NULL);
	ck_assert_int_eq(new->capacity, 1);
	for (i = 0; i < 15; ++i)
		zcore_avlarr_insert(new, items[i].from, &items[i]);
	ck_assert_int_eq(new->count, 15);
	ck_assert(new->capacity >= ZCORE_AVLARR_H2C(3));
	for (i = 0; i < new->capacity; ++i) {
		if (&new->keys[i] != NULL)
			ck_assert_ptr_ne(&new->values[i], NULL);
	}
	zcore_avlarr_destruct(new, NULL, NULL);
	free(new);
END_TEST

static TCase * tcase_maplike_insert(void)
{
	TCase * tc;
	tc = tcase_create("map-like insertion");
	tcase_add_test(tc, maplike_insert_badkey);
	tcase_add_test(tc, maplike_insert_withincap);
	tcase_add_test(tc, maplike_insert_resize);
	return tc;
}

/****************************************************************************
 * MAP-LIKE LOOKUP TEST CASE
 ****************************************************************************/

START_TEST(maplike_lookup)
	struct map_entry * ret;
	size_t i;
	struct zcore_avlarr * new = calloc(1, sizeof(struct zcore_avlarr));
	ck_assert(0 == zcore_avlarr_construct(new, true, 4,
	                                      &mapentry_comparer, NULL));
	ck_assert_ptr_ne(new, NULL);
	for (i = 0; i < 15; ++i)
		zcore_avlarr_insert(new, items[i].from, &items[i]);
	ck_assert(new->count == 15);

	ret = zcore_avlarr_lookup(new, items[_i].from);
	ck_assert_ptr_eq(ret, &items[_i]);

	zcore_avlarr_destruct(new, NULL, NULL);
	free(new);
END_TEST

static TCase * tcase_maplike_lookup(void)
{
	TCase * tc;
	tc = tcase_create("map-like lookup");
	tcase_add_loop_test(tc, maplike_lookup, 0, 15);
	return tc;
}

/****************************************************************************
 * MAP-LIKE REMOVE TEST CASE
 ****************************************************************************/

START_TEST(maplike_remove_badkeys)
	struct map_entry * ret;
	struct zcore_avlarr * new = calloc(1, sizeof(struct zcore_avlarr));
	ck_assert(0 == zcore_avlarr_construct(new, true, 4,
	                                      &mapentry_comparer, NULL));
	ck_assert_ptr_ne(new, NULL);
	ck_assert_int_eq(new->count, 0);

	srand(_i);
	ret = zcore_avlarr_remove(new, (void*)(intptr_t)rand());
	ck_assert_ptr_eq(ret, NULL);
	ck_assert_int_eq(new->count, 0);
	ck_assert(is_ordered(new, 0));
	ck_assert(is_balanced(new));

	zcore_avlarr_destruct(new, NULL, NULL);
	free(new);
END_TEST

START_TEST(maplike_remove_one)
	struct map_entry * ret;
	size_t i;
	struct zcore_avlarr * new = calloc(1, sizeof(struct zcore_avlarr));
	ck_assert(0 == zcore_avlarr_construct(new, true, 4,
	                                      &mapentry_comparer, NULL));
	ck_assert_ptr_ne(new, NULL);
	for (i = 0; i < 15; ++i)
		zcore_avlarr_insert(new, items[i].from, &items[i]);
	ck_assert_int_eq(new->count,15);

	ret = zcore_avlarr_remove(new, items[_i].from);
	ck_assert_ptr_eq(ret, &items[_i]);
	ck_assert_int_eq(new->count, 14);
	ck_assert(is_ordered(new, 0));
	ck_assert(is_balanced(new));

	zcore_avlarr_destruct(new, NULL, NULL);
	free(new);
END_TEST

START_TEST(maplike_remove_random_half)
	struct map_entry * ret;
	size_t i, r, removed=0;
	struct zcore_avlarr * new = calloc(1, sizeof(struct zcore_avlarr));
	ck_assert(0 == zcore_avlarr_construct(new, true, 4,
	                                      &mapentry_comparer, NULL));
	ck_assert_ptr_ne(new, NULL);
	for (i = 0; i < 15; ++i)
		zcore_avlarr_insert(new, items[i].from, &items[i]);
	ck_assert_int_eq(new->count,15);

	srand(_i);
	for(i = 0; i < 7; ++i) {
		r = rand() % 15;
		ret = zcore_avlarr_remove(new, items[r].from);
		if (ret)
			++removed;
		ck_assert_int_eq(new->count, 15-removed);
		ck_assert(is_ordered(new, 0));
		ck_assert(is_balanced(new));
	}
	zcore_avlarr_destruct(new, NULL, NULL);
	free(new);
END_TEST

START_TEST(maplike_remove_random_most)
	struct map_entry * ret;
	size_t i, r, removed=0;
	struct zcore_avlarr * new = calloc(1, sizeof(struct zcore_avlarr));
	ck_assert(0 == zcore_avlarr_construct(new, true, 4,
	                                      &mapentry_comparer, NULL));
	ck_assert_ptr_ne(new, NULL);
	for (i = 0; i < 15; ++i)
		zcore_avlarr_insert(new, items[i].from, &items[i]);
	ck_assert_int_eq(new->count,15);

	srand(_i);
	for (i = 0; i < 12; ++i) {
		r = rand() % 15;
		ret = zcore_avlarr_remove(new, items[r].from);
		if (ret)
			++removed;
		ck_assert_int_eq(new->count, 15-removed);
		ck_assert(is_ordered(new, 0));
		ck_assert(is_balanced(new));
	}
	zcore_avlarr_destruct(new, NULL, NULL);
	free(new);
END_TEST

static TCase * tcase_maplike_remove(void)
{
	TCase * tc;
	tc = tcase_create("map-like remove");
	tcase_add_loop_test(tc, maplike_remove_badkeys, 0, 100);
	tcase_add_loop_test(tc, maplike_remove_one, 0, 15);
	tcase_add_loop_test(tc, maplike_remove_random_half, 0, 100);
	tcase_add_loop_test(tc, maplike_remove_random_most, 0, 100);
	return tc;
}

/****************************************************************************
 * ZCORE_AVLARR EXPORT TESTS
 ****************************************************************************/
#if 0
START_TEST(new_raw_arrays)
	uintptr_t prev;
	size_t i,n;
	void ** keys;
	void ** values;
	struct zcore_avlarr * new = zcore_avlarr_construct(5, NULL, NULL);
	ck_assert_ptr_ne(new, NULL);
	for (i = 1; i <= ZCORE_AVLARR_H2C(4); ++i)
		zcore_avlarr_insert(new, (void*)i, (void*)1);
	ck_assert(new->count == ZCORE_AVLARR_H2C(4));
	ck_assert(is_ordered(new, 0));

	zcore_avlarr_to_raw_arrays(new, &n, &keys, &values);
	ck_assert_int_eq(n, ZCORE_AVLARR_H2C(4));
	ck_assert_ptr_ne(keys, NULL);
	ck_assert_ptr_ne(values, NULL);

	prev = (uintptr_t)keys[0];
	ck_assert_ptr_ne(values[0], NULL);
	for (i = 1; i < n; i++) {
		ck_assert(prev < (uintptr_t)keys[i]);
		prev = (uintptr_t)keys[i];
		ck_assert_ptr_ne(values[i], NULL);
	}

	free(keys);
	free(values);
	zcore_avlarr_destruct(new, NULL, NULL);
END_TEST

START_TEST(new_raw_keys)
	uintptr_t prev;
	size_t i,n;
	void ** keys;
	struct zcore_avlarr * new = zcore_avlarr_construct(5, NULL, NULL);
	ck_assert_ptr_ne(new, NULL);
	for (i = 1; i <= ZCORE_AVLARR_H2C(4); ++i)
		zcore_avlarr_insert(new, (void*)i, (void*)1);
	ck_assert(new->count == ZCORE_AVLARR_H2C(4));
	ck_assert(is_ordered(new, 0));

	zcore_avlarr_to_raw_arrays(new, &n, &keys, NULL);
	ck_assert_int_eq(n, ZCORE_AVLARR_H2C(4));
	ck_assert_ptr_ne(keys, NULL);

	prev = (uintptr_t)keys[0];
	for (i = 1; i < n; i++) {
		ck_assert(prev < (uintptr_t)keys[i]);
		prev = (uintptr_t)keys[i];
	}

	free(keys);
	zcore_avlarr_destruct(new, NULL, NULL);
END_TEST

START_TEST(new_raw_values)
	uintptr_t prev;
	size_t i,n;
	void ** values;
	struct zcore_avlarr * new = zcore_avlarr_construct(5, NULL, NULL);
	ck_assert_ptr_ne(new, NULL);
	for (i = 1; i <= ZCORE_AVLARR_H2C(4); ++i)
		zcore_avlarr_insert(new, (void*)i, (void*)1);
	ck_assert(new->count == ZCORE_AVLARR_H2C(4));
	ck_assert(is_ordered(new, 0));

	zcore_avlarr_to_raw_arrays(new, &n, NULL, &values);
	ck_assert_int_eq(n, ZCORE_AVLARR_H2C(4));
	ck_assert_ptr_ne(values, NULL);

	for (i = 0; i < n; i++) {
		ck_assert_ptr_ne(values[i], NULL);
	}

	free(values);
	zcore_avlarr_destruct(new, NULL, NULL);
END_TEST

START_TEST(ex_raw_arrays)
	uintptr_t prev;
	size_t i, n = 15;
	void * keybuf[16];
	void * valbuf[16];
	void ** keys = &keybuf;
	void ** values = &valbuf;
	struct zcore_avlarr * new = zcore_avlarr_construct(5, NULL, NULL);
	ck_assert_ptr_ne(new, NULL);
	for (i = 1; i <= ZCORE_AVLARR_H2C(4); ++i)
		zcore_avlarr_insert(new, (void*)i, (void*)1);
	ck_assert(new->count == ZCORE_AVLARR_H2C(4));
	ck_assert(is_ordered(new, 0));

	zcore_avlarr_to_raw_arrays(new, &n, &keys, &values);
	ck_assert_int_eq(n, 15);

	prev = (uintptr_t)keys[0];
	ck_assert_ptr_ne(values[0], NULL);
	for (i = 1; i < n; i++) {
		ck_assert(prev < (uintptr_t)keys[i]);
		prev = (uintptr_t)keys[i];
		ck_assert_ptr_ne(values[i], NULL);
	}
	ck_assert_ptr_eq(keys[15],NULL);
	ck_assert_ptr_eq(values[15],NULL);
	zcore_avlarr_destruct(new, NULL, NULL);
END_TEST

static TCase * tcase_exporting(void)
{
	TCase * tc;
	tc = tcase_create("exporting");
	tcase_add_test(tc, new_raw_arrays);
	tcase_add_test(tc, new_raw_keys);
	tcase_add_test(tc, new_raw_values);
	tcase_add_test(tc, ex_raw_arrays);
	return tc;

}
#endif

/****************************************************************************
 * ZCORE_AVLARR OPTIMIZE
 ****************************************************************************/

START_TEST(optimize)
	size_t i, j;
	struct zcore_avlarr * aa = calloc(1, sizeof(struct zcore_avlarr));
	ck_assert(!zcore_avlarr_construct(aa, true, 5, NULL, NULL));
	ck_assert_ptr_ne(aa, NULL);
	srand(_i);
	for (i = 0; i < ZCORE_AVLARR_H2C(5); ++i) {
		do {
			j = rand() % 1000;
		} while(zcore_avlarr_insert(aa, (void*)j, (void*)1));
	}
	ck_assert_int_eq(aa->count, ZCORE_AVLARR_H2C(5));
	int fail = zcore_avlarr_optimize(aa, true);
	ck_assert(!fail);
	ck_assert_int_eq(aa->count, ZCORE_AVLARR_H2C(5));
	ck_assert_int_eq(aa->capacity, ZCORE_AVLARR_H2C(5));
	ck_assert(is_ordered(aa, 0));
	ck_assert(is_full(aa));
	ck_assert(is_balanced(aa));
	zcore_avlarr_destruct(aa, NULL, NULL);
END_TEST

static TCase * tcase_optimize(void)
{
	TCase * tc;
	tc = tcase_create("optimize");
	tcase_add_loop_test(tc, optimize, 0, 100);
	return tc;

}
/****************************************************************************
 * ZCORE_AVLARR SUITE
 ****************************************************************************/

Suite * avlarr_suite(void)
{
	Suite * s;
	s = suite_create("zcore_avlarr");
	suite_add_tcase(s, tcase_allocfree());
	suite_add_tcase(s, tcase_setlike_insert());
	suite_add_tcase(s, tcase_setlike_lookup());
	suite_add_tcase(s, tcase_setlike_remove());
	suite_add_tcase(s, tcase_balance());
	suite_add_tcase(s, tcase_maplike_insert());
	suite_add_tcase(s, tcase_maplike_lookup());
	suite_add_tcase(s, tcase_maplike_remove());
	suite_add_tcase(s, tcase_optimize());

	//suite_add_tcase(s, tcase_exporting());
	//suite_add_tcase(s, tcase_iterator());
	return s;
}
