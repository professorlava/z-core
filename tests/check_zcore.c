#include <stdlib.h>
#include <stdio.h>
#include <check.h>

#include "check_zcore.h"

extern Suite * avlarr_suite(void);

static Suite* (* suiteinit[])(void) = {
	avlarr_suite,
	NULL
};

int main(void)
{
	Suite * s;
	SRunner * sr;
	create_suite_fn * csfn;
	int num_failed = 0;

	for (csfn = suiteinit; *csfn != NULL; ++csfn){
		s  = (*csfn)();
		sr = srunner_create(s);
		srunner_run_all(sr, CK_ENV);
		num_failed += srunner_ntests_failed(sr);
		srunner_free(sr);
	}

	return num_failed;
}
