#ifndef CHECK_ZCORE_H
#define CHECK_ZCORE_H

#include <check.h>

#include "zcore/attributes.h"

typedef Suite * (*create_suite_fn)(void);

#ifndef ck_assert_ptr_eq
#define ck_assert_ptr_eq(a,b) ck_assert((void *)(a)==(void*)(b))
#endif

#ifndef ck_assert_ptr_ne
#define ck_assert_ptr_ne(a,b) ck_assert((void *)(a)!=(void*)(b))
#endif

#endif
