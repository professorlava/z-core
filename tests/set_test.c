#include <stdio.h>

#include <zcore/stdinc.h>
#include <zcore/containers/oset.h>

static struct zcore_oset A;
static struct zcore_oset B;
static struct zcore_oset C;

void _kill(void)
{
	zcore_oset_destruct(&A);
	zcore_oset_destruct(&B);
	zcore_oset_destruct(&C);
}

void _reset(void)
{
	printf("reset\n");
	_kill();
	zcore_oset_construct(&A, NULL, NULL, NULL, NULL);
	zcore_oset_construct(&B, NULL, NULL, NULL, NULL);
	zcore_oset_construct(&C, NULL, NULL, NULL, NULL);
}

void _printset(char * name, struct zcore_oset * A)
{
	printf("%s={ ", name);
	for (size_t i = 0; i < A->avlarr.capacity; ++i) {
		void * elem = A->avlarr.keys[i];
		if (elem)
			printf("%u, ", elem);
	}
	printf("}\n");
}

#define _printsetA() _printset("A", &A)
#define _printsetB() _printset("B", &B)
#define _printsetC() _printset("C", &C)

int main(int argc, char * argv[])
{
	_reset();

	zcore_oset_insert(&A, 1);
	zcore_oset_insert(&A, 8);
	_printsetA();

	zcore_oset_insert(&B, 3);
	zcore_oset_insert(&B, 5);
	_printsetB();

	printf("should be 0: %d\n", zcore_oset_is_subset(&A, &B));
	printf("should be 0: %d\n", zcore_oset_is_subset(&B, &A));
	printf("should be 1: %d\n", zcore_oset_is_disjoint(&A, &B));

	zcore_oset_or_into(&C, &A, &B);
	_printsetC();
	_printsetA();
	_printsetB();

	printf("should be 1: %d\n", zcore_oset_is_subset(&A, &C));
	printf("should be 1: %d\n", zcore_oset_is_subset(&B, &C));
	printf("should be 0: %d\n", zcore_oset_is_subset(&C, &A));
	printf("should be 0: %d\n", zcore_oset_is_subset(&C, &B));
	printf("should be 1: %d\n", zcore_oset_is_proper_subset(&A, &C));
	printf("should be 0: %d\n", zcore_oset_is_proper_subset(&C, &C));

	zcore_oset_or(&A, &B);
	_printsetC();
	_printsetA();

	_reset();

	zcore_oset_insert(&A, 1);
	zcore_oset_insert(&A, 2);
	zcore_oset_insert(&A, 3);
	zcore_oset_insert(&A, 4);
	zcore_oset_insert(&A, 5);
	_printsetA();

	zcore_oset_insert(&B, 4);
	zcore_oset_insert(&B, 5);
	zcore_oset_insert(&B, 6);
	_printsetB();

	zcore_oset_and_into(&C, &A, &B);
	_printsetC();

	zcore_oset_and(&A, &B);
	_printsetA();

	_reset();

	zcore_oset_insert(&A, 1);
	zcore_oset_insert(&A, 2);
	zcore_oset_insert(&A, 3);
	zcore_oset_insert(&A, 4);
	zcore_oset_insert(&A, 5);
	_printsetA();

	zcore_oset_insert(&B, 4);
	zcore_oset_insert(&B, 5);
	zcore_oset_insert(&B, 6);
	_printsetB();

	zcore_oset_sub_into(&C, &A, &B);
	_printsetC();

	zcore_oset_sub(&A, &B);
	_printsetA();

	_reset();

	zcore_oset_insert(&A, 1);
	zcore_oset_insert(&A, 2);
	zcore_oset_insert(&A, 3);
	zcore_oset_insert(&A, 4);
	zcore_oset_insert(&A, 5);
	_printsetA();

	zcore_oset_insert(&B, 4);
	zcore_oset_insert(&B, 5);
	zcore_oset_insert(&B, 6);
	_printsetB();

	zcore_oset_xor_into(&C, &A, &B);
	_printsetC();

	zcore_oset_xor(&A, &B);
	_printsetA();

	_kill();
}




